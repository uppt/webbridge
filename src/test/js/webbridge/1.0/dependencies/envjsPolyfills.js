console.log = this.consoleLogInfo || print;
console.error = this.consoleLogError || print;
console.trace = print;

// local storage fake
if(!window.localStorage) {
    window.localStorage = {
        storage: {},
        setItem: function(key, value) {this.storage[key] = value;},
        getItem: function(key) { return this.storage[key]}
    };
}

// using vert.x websocket client from nashorn JS requires defining a wrapper compatible to JS def of WebSocket
var webSocketClient = null;

function WebSocket(url) {
  var wsImpl = new (Java.type("de.maltebehrendt.uppt.testing.WebSocketClientForJSTest"));
  this.close = function(code, reason) { wsImpl.close(code, reason); };
  this.send = function(message) {wsImpl.send(message);};
  this.readystate = function() { return wsImpl.getReadyState(); };
  this.protocol = "http";
  this.onopen = function() {console.log("WebSocket connection onopen");};
  this.onerror = function() {console.log("WebSocket error" + error);};
  this.onmessage = function() {console.log("WebSocket message" + message);};
  this.onclose = function() {console.log("WebSocket closed");};

  webSocketClient = this;
  wsImpl.connect(url);
};
window.WebSocket = WebSocket;

var wsOnOpen = function() {
  webSocketClient.onopen();
};

var wsOnError = function(error) {
  console.log("WS ERROR: ");
  webSocketClient.onerror(error);
};

var wsOnClose = function() {
  if(webSocketClient.onclose)
    webSocketClient.onclose();
};

var wsOnMessage = function(message) {
  var data = {"data": ["m", message]};
  webSocketClient.onmessage(data);
};

var httpOnResponse = function(response) {
    httpClient.handle(response);
}
var httpOnError = function(code) {
    httpClient.error(code);
}

var httpClient = null;
function XMLHttpRequest() {
    var requestImpl = new (Java.type("de.maltebehrendt.uppt.testing.HTTPPostClientForJSTest"));

    var that = this;
    this.handle = function(response) {
        that.status = 200;
        that.readyState = 4;
        that.responseText = response;
        that.onreadystatechange();
    };
    this.error = function(code) {
        that.readyState = 4;
        that.status = code;
        that.onreadystatechange();
    }

    this.onreadystatechange = function() {};
    this.readyState = 0;
    this.status = 0;
    this.responseText = "";
    this.setRequestHeader = function(key, value) {
        requestImpl.setRequestHeader(key, value);
    };
    this.open = function(method, url, async) {
        requestImpl.open(method, url, async);
    };
    this.send = function(string) {
        requestImpl.send(string);
    };

    httpClient = this;
}
window.XMLHttpRequest = XMLHttpRequest;
/*
function Promise(fn) {
  var that = this;
  this.then = function(resultHandler) { console.log("set rh"); that.resultHandler = resultHandler; return that;};
  this.catch = function(errorHandler) { that.errorHandler = errorHandler; return that;};
  try {
    fn(function(value) {
        console.log("done");
        if(that.resultHandler) {
            that.resultHandler(value);
        }
        else {
            var id = setInterval(function() {
                if(that.resultHandler) {
                    that.resultHandler(value);
                    clearInterval(id);
                }
                else {
                    console.log("then still not available!");
                }
            }, 100);
        }
    }, function(reason) {
        if(that.errorHandler) {
            that.errorHandler(reason);
        }
        else {
            console.log("cause not available!");
        }
    });
  }
  catch(e) {
    console.log("Exception: " + e);
  }
  return this;
};*/


