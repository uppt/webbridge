/**
 * Delegate jasmine test notifications to Java reporter.
 *
 * @author Hendrik Helwich
 * modified by Malte Behrendt
 */
(function(){

    var junitReporter = jasmine.junitReporter;

    var reporter = {
        jasmineStarted: function(options) {
            //print("jasmineStarted "+JSON.stringify(options));
            junitReporter.jasmineStarted(options.totalSpecsDefined);
        },
        jasmineDone: function() {
            //print("jasmineDone");
            junitReporter.jasmineDone();
        },
        suiteStarted: function(result) {
            //print("suiteStarted "+JSON.stringify(result));
            junitReporter.suiteStarted(result.id, result.description, result.fullName, result.status);
        },
        suiteDone: function(result) {
            //print("suiteDone"+JSON.stringify(result));
            junitReporter.suiteDone(result.id, result.description, result.fullName, result.status);
        },
        specStarted: function(result) {
            //print("specStarted"+JSON.stringify(result));
            junitReporter.specStarted(result.id, result.description, result.fullName, result.failedExpectations);
        },
        specDone: function(result) {
            if(result.failedExpectations && result.failedExpectations.length > 0) {
                // replace JSON object with string, so that it can be rea directly in the java reporter
                for(var i=0;i<result.failedExpectations.length;i++) {
                    result.failedExpectations[i] = result.failedExpectations[i].message + "\n\t\t" + result.failedExpectations[i].stack;
                }
            }
            junitReporter.specDone(result.id, result.description, result.fullName, result.failedExpectations, result.status);
        }
    };

    var env = jasmine.getEnv();
    env.addReporter(reporter);

}());