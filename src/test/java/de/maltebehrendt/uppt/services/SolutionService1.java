package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.annotation.Authorities;
import de.maltebehrendt.uppt.annotation.Processor;
import de.maltebehrendt.uppt.annotation.Route;
import de.maltebehrendt.uppt.annotation.Router;
import de.maltebehrendt.uppt.enums.Operation;
import de.maltebehrendt.uppt.events.Message;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.HashMap;

/**
 * Created by malte on 05.03.17.
 */
public class SolutionService1 extends AbstractService {

    // of course, in real life this must not be done state-full but saved in a shared db or sth similar
    private JsonObject testData = null;
    private HashMap<String, String> sessionAddresses = null;

    @Override
    public void prepare(Future<Object> prepareFuture) {
        testData = new JsonObject()
            .put("value", 1234)
            .put("anotherValue", 4321)
            .put("array", new JsonArray().add(1).add(2).add(3))
            .put("object", new JsonObject().put("subValue", 4));

        // in this example stateful, of course it shouldn't be in real applications. And only with userID
        sessionAddresses = new HashMap<>();

        prepareFuture.complete();
    }

    @Router(domain = "in.test",
            version = "1",
            type = "router",
            description = "Minimal test router",
            routes = {
                    @Route(
                            domain = "test",
                            version = "1",
                            path = "some.data.*",
                            operation = Operation.GET
                    ),
                    @Route(
                            domain = "test",
                            version = "1",
                            path = "some.data.*",
                            operation = Operation.SET
                    ),
                    @Route(
                            domain = "test",
                            version = "1",
                            path = "some.data.*",
                            operation = Operation.DEL
                    )
            },
            authorities = @Authorities(
                    isLoginRequired = true,
                    userRoles = {"tester"}
            )
    )
    public void router(Message message) {
        // the results from the results are in the body's "results" JsonArray
        // you can either perform further operations or just return it
        // the user id/roles are accessible as usual via message.userID()/message.userRoles()
        message.reply(message.getBodyAsJsonObject());

        // save the session address for sending push messages (in real applications for pushing changes)
        sessionAddresses.put(message.sessionAddress(), message.userID());
    }

    @Route(
            description = "Performs a get operation in any path ('*') of this domain/version",
            domain = "test",
            version = "1",
            path = "some.data.*",
            operation = Operation.GET
    )
    public void routeGet(Message message) {
        // userID/userRoles are included in the payload/body, not in the header as usual

        JsonArray pathSteps = message.getBodyAsJsonObject().getJsonArray("pathSteps");
        JsonArray pathPredicates = message.getBodyAsJsonObject().getJsonArray("pathPredicates");

        // real scenarios: walk/parse steps and predicates
        // here I know what's being requested...
        String path = null;
        Object value = null;
        if(pathSteps.size() != 3) {
            message.reply(404, "Path steps could not be matched!");
            return;
        }

        switch(pathSteps.getString(2)) {
            case "value":
                path = "some.data.value";
                value = testData.getValue("value");
                break;
            case "anotherValue":
                path = "some.data.anotherValue";
                value = testData.getValue("anotherValue");
                break;
            case "array":
                path = "some.data.array";
                value = testData.getValue("array");
                break;
            case "object":
                path = "some.data.object";
                value = testData.getValue("object");
                break;
            default:
                message.reply(404, "Path steps could not be matched!");
                return;
        }

        message.reply(new JsonObject()
                .put("paths", new JsonArray().add(path))
                .put("values", new JsonArray().add(value))
        );
    }

    @Route(
            description = "Performs a set operation in any path ('*') of this domain/version",
            domain = "test",
            version = "1",
            path = "some.data.*",
            operation = Operation.SET
    )
    public void routeSet(Message message) {
        // userID/userRoles are included in the payload/body, not in the header as usual
        if(!message.getBodyAsJsonObject().getJsonArray("userRoles").contains("tester")) {
            message.reply(401, "User is not allowed to set!");
            return;
        }

        JsonArray pathSteps = message.getBodyAsJsonObject().getJsonArray("pathSteps");
        JsonArray pathPredicates = message.getBodyAsJsonObject().getJsonArray("pathPredicates");
        Object value = message.getBodyAsJsonObject().getValue("value");
        // real scenarios: walk/parse steps and predicates
        // here I know what's being requested...

        if(pathSteps.size() != 3) {
            message.reply(404, "Path steps could not be matched!");
            return;
        }

        testData.put(pathSteps.getString(2), value);

        message.reply(new JsonObject()
                .put("paths", new JsonArray().add("some.data." + pathSteps.getString(2)))
                .put("values", new JsonArray().add(value))
        );
    }

    @Route(
            description = "Performs a del operation in any path ('*') of this domain/version",
            domain = "test",
            version = "1",
            path = "some.data.*",
            operation = Operation.DEL
    )
    public void routeDel(Message message) {
        // userID/userRoles are included in the payload/body, not in the header as usual
        if(!message.getBodyAsJsonObject().getJsonArray("userRoles").contains("tester")) {
            message.reply(401, "User is not allowed to delete!");
            return;
        }

        JsonArray pathSteps = message.getBodyAsJsonObject().getJsonArray("pathSteps");
        JsonArray pathPredicates = message.getBodyAsJsonObject().getJsonArray("pathPredicates");
        // real scenarios: walk/parse steps and predicates
        // here I know what's being requested...

        if(pathSteps.size() != 3) {
            message.reply(404, "Path steps could not be matched!");
            return;
        }

        testData.remove(pathSteps.getString(2));

        message.reply(new JsonObject()
                .put("paths", new JsonArray().add("some.data." + pathSteps.getString(2)))
                .put("values", new JsonArray().add(""))
        );
    }

    @Override
    public void startConsuming() {
        // push a data update every second to all known sessions
        vertx.setPeriodic(1000, time -> {
            for(String sessionAddress : sessionAddresses.keySet()) {
                send("out.userSessions", "1", sessionAddress, new JsonObject()
                        .put("results", new JsonArray()
                                .add(new JsonObject()
                                    .put("domain","testSolution")
                                    .put("version","1")
                                    .put("paths", new JsonArray().add("push.data.value"))
                                    .put("values",new JsonArray().add("foo"))
                                    .put("origin","solutionService1")
                                )
                        ), null);
            }
        });
    }

    @Processor(
            domain = "sessions",
            version = "1",
            type = "end",
            description = "Keeps the list of current sessions which use this router up-to-date (delete session addresses/infos of ended sessions)"
    )
    public void processSessionEnd(Message message) {
        if(sessionAddresses.containsKey(message.getBodyAsJsonObject().getString("sessionAddress"))) {
            sessionAddresses.remove(message.getBodyAsJsonObject().getString("sessionAddress"));
        }
    }


    @Override
    public void shutdown(Future<Object> shutdownFuture) {
        shutdownFuture.complete();
    }
}
