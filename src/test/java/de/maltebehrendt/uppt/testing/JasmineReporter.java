package de.maltebehrendt.uppt.testing;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.runner.Description;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by malte on 07.02.17.
 */
public class JasmineReporter {

    private static Logger logger = LoggerFactory.getLogger(JasmineReporter.class);
    private Async testResult = null;
    private TestContext context = null;
    private List<Description> descriptionStack;
    private int descriptionStackIndex;

    public void jasmineStarted(int totalSpecsDefined) {
        descriptionStack.get(descriptionStackIndex++);
        logger.info("Jasmine started for testing " + totalSpecsDefined + " specs.");
    }

    public void jasmineDone() {
        Description desc = descriptionStack.remove(--descriptionStackIndex);;
        descriptionStack = null;
        logger.info("Jasmine testing done.");
        testResult.countDown();
    }

    public void suiteStarted(String id, String description, String fullName, String status) {
        Description desc = descriptionStack.get(descriptionStackIndex++);
        if (!description.equals(desc.getDisplayName()) || !desc.isSuite()) {
            throw new RuntimeException("unexpected suite description");
        }
        logger.info("Running Jasmine test suite: " + description);
    }

    public void suiteDone(String id, String description, String fullName, String status) {
        Description desc = descriptionStack.remove(--descriptionStackIndex);
        if (!description.equals(desc.getDisplayName()) || !desc.isSuite()) {
            throw new RuntimeException("unexpected suite description");
        }
        logger.info("Completed Jasmine test suite: " + description);
    }

    public void specStarted(String id, String description, String fullName, String[] failedExpectations) {
        Description desc = descriptionStack.get(descriptionStackIndex);
        if (!(description + "()").equals(desc.getDisplayName()) || !desc.isTest()) {
            throw new RuntimeException("unexpected test description");
        }
        logger.debug("Jasmine test started for spec: " + description);
    }

    public void specDone(String id, String description, String fullName, String[] failedExpectations, String status) {
        Description desc = descriptionStack.remove(descriptionStackIndex);
        if (!(description + "()").equals(desc.getDisplayName()) || !desc.isTest()) {
            throw new RuntimeException("unexpected test description");
        }
        context.assertNotNull(status, "A result for spec " + description + " was returned.");

        if ("failed".equals(status)) {
            String failed = "";
            for(String expectation : failedExpectations) {
                failed += "\n\t" + expectation;
            }

            logger.error("Jasmine test FAILED for spec: " + description + failed);

            context.assertTrue(false, "Jasmine test spec " + description + " should not fail.");
        }
        else if ("pending".equals(status)) {
            logger.warn("Jasmine test IGNORED for spec: " + description);
        }
        else {
            logger.debug("Jasmine test completed for spec: " + description);
        }
    }

    public void setDescription(Description description) {
        descriptionStack = new ArrayList<Description>();
        traverse(descriptionStack, description);
        descriptionStackIndex = 0;
    }

    public void setTestResult(Async testResult) {
        this.testResult = testResult;
    }

    public void setContext(TestContext context) {
        this.context = context;
    }

    private void traverse(List<Description> descriptionStack, Description description) {
        descriptionStack.add(description);
        for (Description desc : description.getChildren()) {
            traverse(descriptionStack, desc);
        }
    }
}
