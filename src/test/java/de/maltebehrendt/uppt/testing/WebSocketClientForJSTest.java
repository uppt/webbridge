package de.maltebehrendt.uppt.testing;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.WebSocket;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import javax.script.Invocable;

/**
 * Created by malte on 08.02.17.
 */
public class WebSocketClientForJSTest {
    private static Vertx vertx = null;
    private static Invocable invocable = null;

    private Logger logger = null;
    private short readyState = 0;
    private WebSocket webSocket = null;

    public WebSocketClientForJSTest() {
        logger = LoggerFactory.getLogger(this.getClass());
    }

    public void send(String data) {
        // take care of the weird double escaping...
        data = data.replace("\\", "");
        data = data.substring(2,data.length()-2);
        if(webSocket != null)
            webSocket.writeFinalTextFrame(data);
    }

    public void close(long code, String reason) {
        readyState = 2;
        webSocket.close();
        readyState = 3;
    }

    public void connect(String url) {
        String host = "localhost";
        Integer port = 8080;
        String path = "/eventbus/websocket";

        HttpClient client = vertx.createHttpClient();
        Handler<WebSocket> websocketHandler2;
        client.websocket(port, host, path, websocketHandler -> {
            webSocket = websocketHandler;
            readyState = 1;

            try {
                invocable.invokeFunction("wsOnOpen");
            }
            catch (Exception e) {
                e.printStackTrace();
            }

            webSocket.frameHandler(frame -> {
                if(frame.isFinal()) {
                    if(frame.isText() && invocable != null) {
                        try {
                            invocable.invokeFunction("wsOnMessage", frame.textData());
                        } catch (Exception e) {
                            logger.error("WebSocketClientForJSTest failed!", e);
                        }
                    }
                    else if(frame.isBinary() && invocable != null) {
                        try {
                            invocable.invokeFunction("wsOnMessage", frame.binaryData().toString());
                        } catch (Exception e) {
                            logger.error("WebSocketClientForJSTest failed!", e);
                        }
                    }
                }
            });
            webSocket.closeHandler(closing -> {
                if(invocable != null) {
                    try {
                        readyState = 3;
                        invocable.invokeFunction("wsOnClose");
                    }
                    catch (Exception e) {
                        logger.error("WebSocketClientForJSTest failed!", e);
                    }
                }
            });
            webSocket.exceptionHandler(exception -> {
                logger.error("WebSocketClientForJSTest failed!", exception);

                if(invocable != null) {
                    try {
                        System.err.println(exception.getMessage());
                        invocable.invokeFunction("wsOnError", exception.getMessage());
                    }
                    catch (Exception e) {
                        logger.error("WebSocketClientForJSTest failed!", e);
                    }
                }
            });
        }, failure -> {
            logger.error("WebSocketClientForJSTest failed!", failure);
            
            if(invocable != null) {
                try {
                    invocable.invokeFunction("wsOnError", failure.getMessage());
                }
                catch (Exception e) {
                    logger.error("WebSocketClientForJSTest failed!", e);
                }
            }
        });
    }

    public short getReadyState() {
        return readyState;
    }

    public static void setInvocable(Invocable newInvocable) {
        invocable = newInvocable;
    }
    public static void setVertx(Vertx newVertx) {
        vertx = newVertx;
    }
}
