package de.maltebehrendt.uppt;

import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.apache.commons.codec.binary.Base64;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by malte on 31.01.17.
 */
@RunWith(VertxUnitRunner.class)
public class WebBridgeService1Test {
    private static Vertx vertx = null;
    private static EventBus eventBus = null;

    private static String configDirectory = "test" + File.separator + "webBridgeServiceTest" + File.separator + "local" + File.separator + "config" + File.separator;


    @BeforeClass
    public static void setupLocalVertx(TestContext context) {
        Async setupResult = context.async(1);

        InstanceRunner.setConfigDirectory(configDirectory);
        vertx = InstanceRunner.vertx(new JsonArray().add("userService1.json").add("webBridgeService1.json"), true);
        eventBus = vertx.eventBus();

        MessageConsumer<JsonObject> consumer = eventBus.consumer("system.1.newInstance", message -> {
            // startup of a service complete
            if ("200".equalsIgnoreCase(message.headers().get("statusCode"))) {
                context.assertEquals("200", message.headers().get("statusCode"));
                context.assertNotNull(message.headers().get("correlationID"));
                context.assertNotNull(message.headers().get("origin"));

                setupResult.countDown();
            }
        });

        setupResult.awaitSuccess();
        consumer.unregister();
    }

    @Test
    public void testInAnonymousAccessToUnbridgedAddress(TestContext context) {
        Async requestResult = context.async(2);

        // register an unbridged address
        MessageConsumer<JsonObject> consumer = eventBus.consumer("in.webbridgeservicetest.1.unknown");
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(Message<JsonObject> message) {
            }
        });

        // try to connect to the unbridged address
        HttpClientOptions clientOptions = new HttpClientOptions()
                .setSsl(false)
                .setTrustAll(true);
        HttpClient client = vertx.createHttpClient(clientOptions);
        client.websocket(8080, "localhost", "/eventbus/websocket", websocketHandler -> {
            websocketHandler.frameHandler(frame -> {
                context.assertTrue(frame.isFinal());
                JsonObject message = new JsonObject(frame.textData());
                context.assertEquals("err", message.getString("type"), "Sending to an unbridged address results in an error");
                context.assertEquals("rejected", message.getString("body"), "Sending to an unbridged address results in an rejection error");
                requestResult.countDown();
            });

            // send a message to an unbridged address
            JsonObject message = new JsonObject()
                    .put("type", "send")
                    .put("address", "in.webbridgeservicetest.1.unknown")
                    .put("body", new JsonObject());
            websocketHandler.writeFinalTextFrame(message.encode());
            requestResult.countDown();
        });

        requestResult.awaitSuccess();
        client.close();
    }

    @Test
    public void testInAnonymousAccessToBridgedAddress(TestContext context) {
        Async requestResult = context.async(3);

        // register an  address
        MessageConsumer<JsonObject> consumer = eventBus.consumer("in.webbridgeservicetest.1.bridged");
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(Message<JsonObject> message) {
                context.assertEquals("webUser", message.headers().get("origin"), "MessageImpl origin set");
                context.assertEquals("", message.headers().get("userID"), "User is empty (anonymous)");
                context.assertEquals("[]", message.headers().get("userRoles"), "User is not associated with roles");
                context.assertEquals("WSClient", message.body().getString("source"), "MessageImpl body is included");
                requestResult.countDown();
            }
        });

        JsonObject bridgingRequest = new JsonObject()
                .put("domain", "in.webbridgeservicetest")
                .put("version", "1")
                .put("type", "bridged")
                .put("isInbound", true)
                .put("isOutbound", false)
                .put("authorities", new JsonObject());

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "123456789")
                .addHeader("origin", "WebBridgeServiceTest1");


        eventBus.send("webBridge.1.missingBridge", bridgingRequest, options, reply -> {
            context.assertTrue(reply.succeeded(), "Received reply for new bridge request");
            context.assertEquals("200", reply.result().headers().get("statusCode"), "New bridge request has been processed");
            requestResult.countDown();

            // try to connect to the unbridged address
            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);
            client.websocket(8080, "localhost", "/eventbus/websocket", websocketHandler -> {
                websocketHandler.frameHandler(frame -> {
                    JsonObject message = new JsonObject(frame.textData());
                    //TODO check if message received
                });

                // send a message to an unbridged address
                JsonObject message = new JsonObject()
                        .put("type", "send")
                        .put("address", "in.webbridgeservicetest.1.bridged")
                        .put("body", new JsonObject().put("source", "WSClient"));
                websocketHandler.writeFinalTextFrame(message.encode());
                requestResult.countDown();
                client.close();
            }, failureHandler -> {
                context.assertFalse(true, failureHandler.getMessage());
                failureHandler.printStackTrace();
                client.close();
            });
        });

        requestResult.awaitSuccess();
    }

    @Test
    public void testLoginWithUnknownUser(TestContext context) {
        Async requestResult = context.async(1);

        HttpClientOptions clientOptions = new HttpClientOptions()
                .setSsl(false)
                .setTrustAll(true);
        HttpClient client = vertx.createHttpClient(clientOptions);
        HttpClientRequest request = client.post(8080,"localhost", "/email/login", response -> {
            context.assertEquals(404, response.statusCode(), "Status code is as expected");
            requestResult.countDown();
        });
        String body = new JsonObject().put("email", "test1@test.de").put("password", "reallySafe").encode();
        request.putHeader(HttpHeaders.CONTENT_TYPE, "application/json").end(body);
        requestResult.awaitSuccess();
        client.close();
    }

    @Test
    public void testLoginWithUnknownUserAndNoPassword(TestContext context) {
        Async requestResult = context.async(1);

        HttpClientOptions clientOptions = new HttpClientOptions()
                .setSsl(false)
                .setTrustAll(true);
        HttpClient client = vertx.createHttpClient(clientOptions);
        HttpClientRequest request = client.post(8080,"localhost", "/email/login", response -> {
            context.assertEquals(400, response.statusCode(), "Status code is as expected");
            requestResult.countDown();
        });
        String body = new JsonObject().put("email", "test1@test.de").encode();
        request.putHeader(HttpHeaders.CONTENT_TYPE, "application/json").end(body);
        requestResult.awaitSuccess();
        client.close();
    }

    @Test
    public void testLoginWithNewUser(TestContext context) {
        Async requestResult = context.async(3);

        vertx.executeBlocking(future -> {
            createNewUser(null, future);
        }, result -> {
            context.assertTrue(result.succeeded(), "New user was created");
            JsonObject user = (JsonObject) result.result();
            requestResult.countDown();

            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);
            HttpClientRequest request = client.post(8080,"localhost", "/email/login", response -> {
                context.assertEquals(200, response.statusCode(), "Status code is as expected");
                response.bodyHandler(totalBuffer -> {
                    JsonObject responseData = totalBuffer.toJsonObject();
                    context.assertNotNull(responseData, "Post response contains a non-empty JSON Object");
                    context.assertNotNull(responseData.getString("xSessionToken"), "Responde contains a token");
                    requestResult.countDown();

                    vertx.executeBlocking(delFuture -> {
                        deleteUser(user.getString("email"), delFuture);
                    }, delResult -> {
                        context.assertTrue(delResult.succeeded(), "New user was deleted again");
                        requestResult.countDown();
                        client.close();
                    });
                });
            });
            String body = new JsonObject().put("email", user.getString("email")).put("password", user.getString("password")).encode();
            request.putHeader(HttpHeaders.CONTENT_TYPE, "application/json").end(body);
        });
        requestResult.awaitSuccess();
    }


    @Test
    public void testInAuthenticatedRoleAccessToPublicBridgeAddress(TestContext context) {
        Async requestResult = context.async(4);

        // register an  address
        MessageConsumer<JsonObject> consumer = eventBus.consumer("in.webbridgeservicetest.1.bridgedTest2");
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(Message<JsonObject> message) {
                context.assertEquals("webUser", message.headers().get("origin"), "MessageImpl origin set");
                context.assertTrue(message.headers().get("userID").indexOf("@") > 0, "User id is provided via his email");
                context.assertEquals("[\"tester\",\"superTester\"]", message.headers().get("userRoles"), "User is associated with its roles");
                context.assertEquals("WSClient", message.body().getString("source"), "MessageImpl body is included");
                requestResult.countDown();
            }
        });


        JsonObject bridgingRequest = new JsonObject()
                .put("domain", "in.webbridgeservicetest")
                .put("version", "1")
                .put("type", "bridgedTest2")
                .put("isInbound", true)
                .put("isOutbound", false)
                .put("authorities", new JsonObject());

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "123456789")
                .addHeader("origin", "WebBridgeServiceTest1");


        eventBus.send("webBridge.1.missingBridge", bridgingRequest, options, reply -> {
            context.assertTrue(reply.succeeded(), "Received reply for new bridge request");
            context.assertEquals("200", reply.result().headers().get("statusCode"), "New bridge request has been processed");
            requestResult.countDown();

            vertx.executeBlocking(future -> {
                loginWithNewUser(null, future);
            }, result -> {

                HashMap<String, Object> loginMap = (HashMap<String, Object>) result.result();
                HttpClient client = (HttpClient) loginMap.get("client");
                String token = (String) loginMap.get("token");
                requestResult.countDown();

                client.websocket(8080, "localhost", "/eventbus/websocket", websocketHandler -> {
                    websocketHandler.frameHandler(frame -> {
                        JsonObject message = new JsonObject(frame.textData());
                    });
                    // send a message to the bridged address
                    JsonObject message = new JsonObject()
                            .put("type", "send")
                            .put("address", "in.webbridgeservicetest.1.bridgedTest2")
                            .put("body", new JsonObject().put("source", "WSClient").put("xSessionToken", token));
                    websocketHandler.writeFinalTextFrame(message.encode());
                    requestResult.countDown();
                });
            });
        });

        requestResult.awaitSuccess();
    }

    @Test
    public void testInAuthorizedRoleAccessToBridgedAddress(TestContext context) {
        Async requestResult = context.async(3);

        // register an  address
        MessageConsumer<JsonObject> consumer = eventBus.consumer("in.webbridgeservicetest.1.bridgedTest3");
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(Message<JsonObject> message) {

                context.assertEquals("webUser", message.headers().get("origin"), "MessageImpl origin set");
                context.assertTrue(message.headers().get("userID").indexOf("@") > 0, "User id is provided via his email");
                context.assertEquals("[\"tester\",\"superTester\"]", message.headers().get("userRoles"), "User is associated with its roles");
                context.assertEquals("WSClient", message.body().getString("source"), "MessageImpl body is included");
                requestResult.countDown();
            }
        });


        JsonObject bridgingRequest = new JsonObject()
                .put("domain", "in.webbridgeservicetest")
                .put("version", "1")
                .put("type", "bridgedTest3")
                .put("isInbound", true)
                .put("isOutbound", false)
                .put("authorities", new JsonObject().put("roles", new JsonArray().add("tester")));

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "123456789")
                .addHeader("origin", "WebBridgeServiceTest1");

        eventBus.send("webBridge.1.missingBridge", bridgingRequest, options, reply -> {
            context.assertTrue(reply.succeeded(), "Received reply for new bridge request");
            context.assertEquals("200", reply.result().headers().get("statusCode"), "New bridge request has been processed");
            requestResult.countDown();

            vertx.executeBlocking(future -> {
                loginWithNewUser(null, future);
            }, result -> {
                HashMap<String, Object> loginMap = (HashMap<String, Object>) result.result();
                HttpClient client = (HttpClient) loginMap.get("client");
                String token = (String) loginMap.get("token");

                client.websocket(8080, "localhost", "/eventbus/websocket", websocketHandler -> {
                    websocketHandler.frameHandler(frame -> {
                        JsonObject message = new JsonObject(frame.textData());
                    });
                    // send a message to the bridged address
                    JsonObject message = new JsonObject()
                            .put("type", "send")
                            .put("address", "in.webbridgeservicetest.1.bridgedTest3")
                            .put("body", new JsonObject().put("source", "WSClient").put("xSessionToken", token));
                    websocketHandler.writeFinalTextFrame(message.encode());
                    requestResult.countDown();
                });
            });
        });
        requestResult.awaitSuccess();
    }

    @Test
    public void testInUnauthorizedRoleAccessToBridgedAddress(TestContext context) {
        Async requestResult = context.async(3);

        // register an  address
        MessageConsumer<JsonObject> consumer = eventBus.consumer("in.webbridgeservicetest.1.bridgedTest4");
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(Message<JsonObject> message) {

            }
        });


        JsonObject bridgingRequest = new JsonObject()
                .put("domain", "in.webbridgeservicetest")
                .put("version", "1")
                .put("type", "bridgedTest4")
                .put("isInbound", true)
                .put("isOutbound", false)
                .put("authorities", new JsonObject().put("roles", new JsonArray().add("impossibleRoleToHave")));

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "123456789")
                .addHeader("origin", "WebBridgeServiceTest1");

        eventBus.send("webBridge.1.missingBridge", bridgingRequest, options, reply -> {
            context.assertTrue(reply.succeeded(), "Received reply for new bridge request");
            context.assertEquals("200", reply.result().headers().get("statusCode"), "New bridge request has been processed");
            requestResult.countDown();

            vertx.executeBlocking(future -> {
                loginWithNewUser(null, future);
            }, result -> {
                HashMap<String, Object> loginMap = (HashMap<String, Object>) result.result();
                HttpClient client = (HttpClient) loginMap.get("client");
                String token = (String) loginMap.get("token");

                client.websocket(8080, "localhost", "/eventbus/websocket", websocketHandler -> {
                    websocketHandler.frameHandler(frame -> {
                        context.assertTrue(frame.isFinal());
                        JsonObject message = new JsonObject(frame.textData());
                        context.assertEquals("err", message.getString("type"), "Sending to an unbridged address results in an error");
                        context.assertEquals("rejected", message.getString("body"), "Sending to an unbridged address results in an rejection error");
                        requestResult.countDown();
                    });

                    // send a message to the bridged address
                    JsonObject message = new JsonObject()
                            .put("type", "send")
                            .put("address", "in.webbridgeservicetest.1.bridgedTest4")
                            .put("body", new JsonObject().put("source", "WSClient").put("xSessionToken", token));
                    websocketHandler.writeFinalTextFrame(message.encode());
                    requestResult.countDown();
                });
            });
        });
        requestResult.awaitSuccess();
    }

    @Test
    public void testInBridgingMalformattedAddress(TestContext context) {
        Async requestResult = context.async(1);

        JsonObject bridgingRequest = new JsonObject()
                .put("domain", "webbridgeservicetest")
                .put("version", "1")
                .put("type", "bridged")
                .put("isInbound", true)
                .put("isOutbound", false)
                .put("authorities", new JsonObject());

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "987654321")
                .addHeader("origin", "WebBridgeServiceTest1");


        eventBus.send("webBridge.1.missingBridge", bridgingRequest, options, reply -> {
            context.assertTrue(reply.succeeded(), "Received reply for new bridge request");
            context.assertEquals("400", reply.result().headers().get("statusCode"), "New bridge request has been rejected");
            requestResult.countDown();
        });

        requestResult.awaitSuccess();
    }

    @Test
    public void testInObsoleteBridge(TestContext context) {
        Async requestResult = context.async(3);

        JsonObject bridgingRequest = new JsonObject()
                .put("domain", "in.webbridgeserviceobsoletetest")
                .put("version", "1")
                .put("type", "bridged")
                .put("isInbound", true)
                .put("isOutbound", false)
                .put("authorities", new JsonObject());

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "9876543210")
                .addHeader("origin", "WebBridgeServiceTest1");

        eventBus.send("webBridge.1.missingBridge", bridgingRequest, options, reply -> {
                context.assertTrue(reply.succeeded(), "Received reply for new bridge request");
                context.assertEquals("200", reply.result().headers().get("statusCode"), "New bridge request has been processed");
                requestResult.countDown();

                eventBus.send("webBridge.1.obsoleteBridge", new JsonObject()
                        .put("domain", "in.webbridgeserviceobsoletetest")
                        .put("version", "1")
                        .put("type", "bridged"), options, obsoleteReply-> {

                    context.assertTrue(obsoleteReply.succeeded(), "Received reply for obsolete bridge request");
                    context.assertEquals("200", obsoleteReply.result().headers().get("statusCode"), "Obsolete bridge request has been processed");
                    requestResult.countDown();

                    eventBus.send("webBridge.1.obsoleteBridge", new JsonObject()
                            .put("domain", "in.webbridgeserviceobsoletetest")
                            .put("version", "1")
                            .put("type", "bridged"), options, duplicateObsoleteReply-> {

                        context.assertTrue(duplicateObsoleteReply.succeeded(), "Received reply for duplicate obsolete bridge request");
                        context.assertEquals("404", duplicateObsoleteReply.result().headers().get("statusCode"), "Duplicate obsolete bridge request has been processed");
                        requestResult.countDown();
                    });
                });
            });

        requestResult.awaitSuccess();
    }

    private void loginWithNewUser(JsonArray roles, Future<Object> future) {
        vertx.executeBlocking(loginFuture -> {
            createNewUser(roles, loginFuture);
        }, result -> {
            JsonObject user = (JsonObject) result.result();
            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);
            HttpClientRequest request = client.post(8080,"localhost", "/email/login", response -> {
                response.bodyHandler(totalBuffer -> {
                    JsonObject responseData = totalBuffer.toJsonObject();
                    HashMap<String,Object> loginMap = new HashMap<>();
                    loginMap.put("token", responseData.getString("xSessionToken"));
                    loginMap.put("client", client);
                    future.complete(loginMap);
                });
            });
            String body = new JsonObject().put("email", user.getString("email")).put("password", user.getString("password")).encode();
            request.putHeader(HttpHeaders.CONTENT_TYPE, "application/json").end(body);
        });
    }

    private void createNewUser(JsonArray roles, Future<Object> future) {
        int randomNumber = ThreadLocalRandom.current().nextInt(1, 5001);

        JsonObject newEMailUser = new JsonObject()
                .put("email", "test" + randomNumber +"@test.de")
                .put("passwordKeyFactory", "PBKDF2WithHmacSHA512")
                .put("passwordSalt", "testSaltToping")
                .put("passwordIterations", 10)
                .put("passwordKeyLength", 256);

        if(roles != null) {
            newEMailUser.put("roles", roles);
        }
        else {
            newEMailUser.put("roles", new JsonArray().add("tester").add("superTester"));
        }

        String password = "superSecret12$%&\\234?.;.°" + randomNumber;
        String passwordHash = hashPassword(password,
                newEMailUser.getString("passwordKeyFactory"),
                newEMailUser.getString("passwordSalt"),
                newEMailUser.getInteger("passwordIterations"),
                newEMailUser.getInteger("passwordKeyLength"));
        newEMailUser.put("passwordHash", passwordHash);
        newEMailUser.put("password", password);

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "858518121")
                .addHeader("origin", "WebBridgeService1Test");

        eventBus.send("users.1.newEMailUser", newEMailUser, options, reply -> {
            if(reply.failed() || !"200".equals(reply.result().headers().get("statusCode"))) {
                future.fail(reply.cause());
            }
            else {
                future.complete(newEMailUser);
            }
        });
    }

    private void deleteUser(String id, Future<Object> future) {
        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "858518121")
                .addHeader("origin", "WebBridgeService1Test");

        eventBus.send("users.1.userBecameObsolete",new JsonObject().put("id", id), options, delReply -> {
            if(delReply.failed() || !"200".equals(delReply.result().headers().get("statusCode"))) {
                future.fail(delReply.cause());
            }
            else {
                future.complete(id);
            }
        });
    }

    private static String hashPassword(final String password, final String keyFactory, final String saltString, final int iterations, final int keyLength) {
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance( keyFactory );
            byte[] salt = saltString.getBytes("UTF-8");
            PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, keyLength);
            SecretKey key = skf.generateSecret( spec );
            byte[] res = key.getEncoded( );
            return new Base64().encodeAsString(res);

        } catch( NoSuchAlgorithmException | InvalidKeySpecException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @AfterClass
    public static void tearDownVertx(TestContext context) {
        try {
            vertx.close();
        }
        catch(Exception e) {}
    }
}
