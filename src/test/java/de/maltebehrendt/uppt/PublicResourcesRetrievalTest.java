package de.maltebehrendt.uppt;

import de.maltebehrendt.uppt.events.Impl.MessageImpl;
import de.maltebehrendt.uppt.util.FileUtils;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.file.AsyncFile;
import io.vertx.core.file.OpenOptions;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.net.JksOptions;
import io.vertx.core.net.NetClient;
import io.vertx.core.net.NetClientOptions;
import io.vertx.core.streams.Pump;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.UUID;

/**
 * Created by malte on 04.02.17.
 */
@RunWith(VertxUnitRunner.class)
public class PublicResourcesRetrievalTest {

    private static Vertx vertx = null;
    private static EventBus eventBus = null;

    private static String configDirectory = "test" + File.separator + "publicResourcesRetrieval" + File.separator + "config" + File.separator;
    private static String webRootDirectory = "test/publicResourcesRetrieval/webroot";


    @BeforeClass
    public static void setupLocalVertx(TestContext context) {
        Async setupResult = context.async(1);

        // clean up root folder
        FileUtils.deleteFolder(webRootDirectory);
        File targetFolder = new File(webRootDirectory);
        targetFolder.mkdir();

        InstanceRunner.setConfigDirectory(configDirectory);
        vertx = InstanceRunner.vertx(new JsonArray().add("userService1.json").add("webBridgeService1.json").add("noopService1.json"), true);
        eventBus = vertx.eventBus();

        MessageConsumer<JsonObject> consumer = eventBus.consumer("system.1.newInstance", message -> {
            // startup of a service complete
            if ("200".equalsIgnoreCase(message.headers().get("statusCode"))) {
                context.assertEquals("200", message.headers().get("statusCode"));
                context.assertNotNull(message.headers().get("correlationID"));
                context.assertNotNull(message.headers().get("origin"));

                setupResult.countDown();
            }
        });

        setupResult.awaitSuccess();
        consumer.unregister();
    }

    @Test
    public void testIfPublicResourcesAvailableOnStorage(TestContext context) {
        // check that the files exist on storage
        File targetDirectory = new File(webRootDirectory + File.separator + "noop" + File.separator + "1");
        context.assertTrue(targetDirectory.exists() && targetDirectory.isDirectory(), "Target path exists");
        context.assertEquals(1, targetDirectory.listFiles().length, "Target path contains one entry");

        File folder = targetDirectory.listFiles()[0];
        context.assertEquals("vertx", folder.getName(), "Target entry name is correct");
        context.assertEquals(4, folder.listFiles().length, "Target entry content count is correct");
    }

    @Test
    public void testIfPublicResourcesAvailableViaHTTP(TestContext context) {
        Async testResult = context.async(2);

        // check that resources are available via HTTPS - or in this case a least one file :-)
        JksOptions jksOptions =  new JksOptions().setPath("test/publicResourcesRetrieval/config/services/webBridgeService/sslKeyStore.jks").setPassword("strongPassword");
        HttpClientOptions clientOptions = new HttpClientOptions()
                .setSsl(false)
                .setTrustAll(true);
        HttpClient client = vertx.createHttpClient(clientOptions);

        client.getNow(8080, "localhost", "/public/noop/1/vertx/vertx-stack.json", responseHandler -> {
            context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

            responseHandler.bodyHandler(body -> {
                String fileContent = body.toString();
                context.assertEquals(8858, fileContent.length(), "Returned file has the correct length");
                testResult.countDown();
            });

        });

        client.getNow(8080, "localhost", "/public/noop/1/vertx/vertx-stack.non.json", responseHandler -> {
            context.assertEquals(404, responseHandler.statusCode(), "Requesting non-existent files causes a 404");
            testResult.countDown();
        });

        testResult.awaitSuccess();
        client.close();
    }

    @Test
    public void testNewPublicResourceZipRetrieval(TestContext context) {
        Async testResult = context.async(6);

        String serviceID = UUID.randomUUID().toString();


        MessageConsumer<JsonObject> consumer = eventBus.consumer("publicResources.1." + serviceID, msg -> {
            de.maltebehrendt.uppt.events.Message message = new MessageImpl(msg);
            // startup of a service complete
            context.assertEquals(100, message.statusCode());
            context.assertEquals("vert.x-3.5.0.zip", message.getBodyAsJsonObject().getString("fileName"));

            testResult.countDown();

            vertx.executeBlocking(future -> {
                sendFileViaTCP(message, "test/publicResourcesRetrieval/vert.x-3.5.0.zip", future);
            }, sendResult -> {
                context.assertFalse(sendResult.failed());
                if(sendResult.failed()) {
                    System.err.println("[" + message.correlationID() + "] Failed to get confirmation for sending public resource file vert.x-3.5.0.zip to " + message.origin());
                    return;
                }
                MessageImpl sendReply = (MessageImpl) sendResult.result();
                context.assertEquals(201, sendReply.statusCode());
                if(sendReply.statusCode() != 201) {
                    System.err.println("[" + message.correlationID() + "] Failed to send public resource file vert.x-3.5.0.zip to " + message.origin() + ": " + message.getMessage());
                    return;
                }
                testResult.countDown();
                // request unpack confirmation
                sendReply.reply(200, processingConfirmation -> {
                    MessageImpl confirmation = (MessageImpl) processingConfirmation;
                    if(confirmation.statusCode() != 200) {
                        System.err.println("[" + message.correlationID() + "] Public resource file vert.x-3.5.0.zip unpack at " + message.origin() + " failed: " + message.getMessage());
                        return;
                    }
                    testResult.countDown();

                    // check that the files exist on storage
                    File targetDirectory = new File(webRootDirectory + File.separator + "noop" + File.separator + "2");
                    context.assertTrue(targetDirectory.exists() && targetDirectory.isDirectory(), "Target path exists");
                    context.assertEquals(1, targetDirectory.listFiles().length, "Target path contains one entry");

                    File folder = targetDirectory.listFiles()[0];
                    context.assertEquals("vertx", folder.getName(), "Target entry name is correct");
                    context.assertEquals(4, folder.listFiles().length, "Target entry content count is correct");
                    testResult.countDown();

                    // check if file available via HTTP
                    JksOptions jksOptions =  new JksOptions().setPath("test/publicResourcesRetrieval/config/services/webBridgeService/sslKeyStore.jks").setPassword("strongPassword");
                    HttpClientOptions clientOptions = new HttpClientOptions()
                            .setSsl(false)
                            .setTrustAll(true);
                    HttpClient client = vertx.createHttpClient(clientOptions);

                    client.getNow(8080, "localhost", "/public/noop/2/vertx/vertx-stack.json", responseHandler -> {
                        context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

                        responseHandler.bodyHandler(body -> {
                            String fileContent = body.toString();
                            context.assertEquals(11644, fileContent.length(), "Returned file has the correct length");
                            testResult.countDown();
                        });

                    });

                    client.getNow(8080, "localhost", "/public/noop/2/vertx/vertx-stack.non.json", responseHandler -> {
                        context.assertEquals(404, responseHandler.statusCode(), "Requesting non-existent files causes a 404");
                        testResult.countDown();
                        client.close();
                    });
                });
            });
        });

        eventBus.publish("publicResources.1.newResourceZip", new JsonObject()
                .put("fileName", "vert.x-3.5.0.zip")
                .put("relativeExtractionPath", "noop/2/")
                .put("serviceDomain", "publicResources")
                .put("serviceVersion", "1")
                .put("serviceType", serviceID)
        );

        testResult.awaitSuccess();
        consumer.unregister();
    }

    /*

    @Test
    public void testHandleNewStaticRessourceZip(TestContext context) {
        Async testResult = context.async(3);

        String correlationID = UUID.randomUUID().toString();

        JsonObject ressourceInfo = new JsonObject()
                .put("fileName","vert.x-3.3.3.zip")
                .put("relativeExtractionPath","staticRessourceRetrieval1")
                .put("serviceDomain","test")
                .put("serviceType","missingRessourcesZip1")
                .put("serviceVersion","1")
                .put("origin","testHandleNewStaticRessourceZip1");

        NoopService1 noopService = new NoopService1();
        noopService.setLogger(LoggerFactory.getLogger(NoopService1.class));
        noopService.setLanAddress(NoopService1.getLANAddress().getHostAddress());
        noopService.setVertx(vertx);

        MessageConsumer<JsonObject> consumer = eventBus.consumer("test.1.missingRessourcesZip1");
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(io.vertx.core.eventbus.Message<JsonObject> message) {
                MessageImpl upptMessageImpl = new MessageImpl(message);

                context.assertEquals(100, upptMessageImpl.statusCode(), "Status code is correct");
                JsonObject payload = upptMessageImpl.getBodyAsJsonObject();
                context.assertEquals("vert.x-3.3.3.zip", payload.getString("fileName"), "Requested file is correct");
                testResult.countDown();

                vertx.executeBlocking(future -> {
                    noopService.sendFileViaTCP(upptMessageImpl, sourceFile, future);
                }, result -> {
                    context.assertTrue(result.succeeded(), "Sending file seems to be successful");
                    MessageImpl sendReply = (MessageImpl) result.result();
                    context.assertEquals(201, sendReply.statusCode(), "File was received and is being processed");
                    testResult.countDown();

                    // request unpack confirmation
                    sendReply.reply(200, processingConfirmation -> {
                        MessageImpl confirmation = (MessageImpl) processingConfirmation;
                        context.assertEquals(200, confirmation.statusCode(), "Ressources were unpacked successfully and are available now");
                        testResult.countDown();
                    });
                });
            }
        });

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", correlationID)
                .addHeader("origin", "testStaticRessourceRetrieval1");

        eventBus.publish("webBridge.1.newStaticRessourceZip", ressourceInfo, options);
        testResult.awaitSuccess();
    }

    @Test
    public void testNoopServiceResourceAvailability(TestContext context) {
        Async testResult = context.async(5);

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "456456")
                .addHeader("origin", "testNoopServiceRessourceAvailability");
        eventBus.send("webBridge.1.missingStaticRessourcesAvailabilityInfo", new JsonObject(), options, reply -> {
            context.assertTrue(reply.succeeded());
            JsonArray list = ((JsonObject) reply.result().body()).getJsonArray("availabilityList");
            context.assertNotNull(list, "Availability list was returned");
            context.assertFalse(list.isEmpty(), "Availability list is not empty");
            testResult.countDown();

            // check that the WebBridge says the noop services's resources available...
            for(int i=0;i<list.size();i++) {
                JsonObject info = list.getJsonObject(i);
                if(!"de.maltebehrendt.uppt.services.NoopService1".equals(info.getString("sourceOrigin"))
                        || !"noop/1/".equals(info.getString("locationPath"))) {
                    continue;
                }

                context.assertEquals("de.maltebehrendt.uppt.services.WebBridgeService1", info.getString("locationService"), "locationService is set correctly");
                context.assertEquals("de.maltebehrendt.uppt.services.NoopService1", info.getString("sourceOrigin"), "sourceOrigin is set correctly");
                context.assertNotNull(info.getString("locationHost"), "locationHost is set");
                context.assertEquals("vert.x-3.3.3.zip", info.getString("sourceFile"), "sourceFile is set correctly");
                context.assertEquals("noop/1/", info.getString("locationPath"), "locationPath is set correctly");
                testResult.countDown();

                // check that the files exist on storage
                File targetDirectory = new File(webRootDirectory + File.separator + info.getString("locationPath"));
                context.assertTrue(targetDirectory.exists() && targetDirectory.isDirectory(), "Target path exists");
                context.assertEquals(1, targetDirectory.listFiles().length, "Target path contains one entry");

                File folder = targetDirectory.listFiles()[0];
                context.assertEquals("vertx", folder.getName(), "Target entry name is correct");
                context.assertEquals(4, folder.listFiles().length, "Target entry content count is correct");

                testResult.countDown();

                // check that resources are available via HTTPS - or in this case a least one file :-)
                HttpClientOptions clientOptions = new HttpClientOptions()
                        .setSsl(true)
                        .setTrustAll(true);
                HttpClient client = vertx.createHttpClient(clientOptions);
                client.getNow(8080, "localhost", "/static/" + info.getString("locationPath") + "/vertx/vertx-stack.json", responseHandler -> {
                    context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

                    responseHandler.bodyHandler(body -> {
                        String fileContent = body.toString();
                        context.assertEquals(8858, fileContent.length(), "Returned file has the correct length");
                        testResult.countDown();
                    });

                });

                client.getNow(8080, "localhost", "/static/" + info.getString("locationPath") + "/vertx/vertx-stack.non.json", responseHandler -> {
                    context.assertEquals(404, responseHandler.statusCode(), "Requesting non-existent files causes a 404");
                    testResult.countDown();
                });
            }
        });

        testResult.awaitSuccess();
    }

    @Test
    public void testLaterStartedNoopServiceResourceAvailability(TestContext context) {
        Async testResult = context.async(4);


        // prepare deploying a new service with resources
        JsonObject noopService1Config = new JsonObject()
                .put("serviceLocation", "de.maltebehrendt.uppt.services.NoopService1")
                .put("serviceInstances", 1)
                .put("hasStaticFiles", true)
                .put("staticFiles", new JsonObject()
                    .put("vert.x-3.3.3.zip", new JsonObject()
                        .put("filePath", sourceFile)
                        .put("relativeExtractionPath", "delayedNoop/1/")
                    )
                );

        Future<String> deployFuture = Future.future();
        Handler<AsyncResult<String>> deploymentHandler = new Handler<AsyncResult<String>>() {
            @Override
            public void handle(AsyncResult<String> event) {
                context.assertTrue(deployFuture.succeeded(), "Delayed service was deployed");
                testResult.countDown();
            }
        };
        deployFuture.setHandler(deploymentHandler);

        // setup listener for new resource availability
        eventBus.consumer("webBridge.1.newStaticRessourcesAvailabilityInfo", handler -> {
            MessageImpl message = new MessageImpl(handler);
            JsonObject info = message.getBodyAsJsonObject();

            context.assertEquals("de.maltebehrendt.uppt.services.WebBridgeService1", info.getString("locationService"), "locationService is set correctly");
            context.assertEquals("de.maltebehrendt.uppt.services.NoopService1", info.getString("sourceOrigin"), "sourceOrigin is set correctly");
            context.assertNotNull(info.getString("locationHost"), "locationHost is set");
            context.assertEquals("vert.x-3.3.3.zip", info.getString("sourceFile"), "sourceFile is set correctly");
            context.assertEquals("delayedNoop/1/", info.getString("locationPath"), "locationPath is set correctly");
            testResult.countDown();

            // check that the files exist on storage
            File targetDirectory = new File(webRootDirectory + File.separator + info.getString("locationPath"));
            context.assertTrue(targetDirectory.exists() && targetDirectory.isDirectory(), "Target path exists");
            context.assertEquals(1, targetDirectory.listFiles().length, "Target path contains one entry");

            File folder = targetDirectory.listFiles()[0];
            context.assertEquals("vertx", folder.getName(), "Target entry name is correct");
            context.assertEquals(4, folder.listFiles().length, "Target entry content count is correct");

            testResult.countDown();

            // check that resources are available via HTTPS - or in this case a least one file :-)
            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(true)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);
            client.getNow(8080, "localhost", "/static/" + info.getString("locationPath") + "/vertx/vertx-stack.json", responseHandler -> {
                context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

                responseHandler.bodyHandler(body -> {
                    String fileContent = body.toString();
                    context.assertEquals(8858, fileContent.length(), "Returned file has the correct length");
                    testResult.countDown();
                });

            });
        });

        // deploy service
        InstanceRunner.deployVerticle(noopService1Config, deployFuture);

        testResult.awaitSuccess();
    }
*/

    // adopted from AbstractService.java
    private void sendFileViaTCP(de.maltebehrendt.uppt.events.Message message, String filePath, Future<Object> sendFuture) {
        if(filePath == null || filePath.contains("..") || filePath.matches("[:\"'*?<>|]+")) {
            sendFuture.fail("File path was not provided or is invalid (it must not contain '..' or '[:\"'*?<>|]+'): " + filePath);
            return;
        }
        File file = new File(filePath);
        if(!file.exists() || !file.isFile()) {
            sendFuture.fail("File does not exist or is not a file: " + filePath);
            return;
        }
        if(!file.canRead()) {
            sendFuture.fail("No read access for file: " + filePath);
            return;
        }

        Handler<de.maltebehrendt.uppt.events.Message<Object>> reply = reply1 -> {
            if(reply1.statusCode() != 101) {
                sendFuture.fail(reply1.getBodyAsJsonObject().getString("message"));
                return;
            }
            vertx.fileSystem().open(filePath, new OpenOptions(), asyncFile -> {
                if(asyncFile.failed()) {
                    System.err.println("[" + reply1.correlationID() + "] Failed to open file: " + filePath);
                    reply1.reply(500, new JsonObject().put("message", "Failed to open file."));
                    sendFuture.fail("Failed to open file: " + filePath);
                    return;
                }
                final AsyncFile sourceFile = asyncFile.result();

                String remoteAddress = reply1.getBodyAsJsonObject().getString("address");
                Integer remotePort = reply1.getBodyAsJsonObject().getInteger("port");

                // connect via a tcp client...
                NetClient client = vertx.createNetClient(new NetClientOptions().
                        setReconnectAttempts(5).
                        setReconnectInterval(250));

                client.connect(remotePort, remoteAddress, connectionResult -> {
                    if(connectionResult.failed()) {
                        System.err.println("[" + reply1.correlationID() + "] Failed to startup TCP client for sending file " + filePath + " to " + reply1.origin() + " at " + remoteAddress);
                        sendFuture.fail("Failed to startup TCP Server for receiving file!");
                        return;
                    }
                    reply1.reply(200, processingReply -> {
                        if(processingReply.statusCode() != 201) {
                            sendFuture.fail(processingReply.getBodyAsJsonObject().getString("message"));
                        }
                        else {
                            sendFuture.complete(processingReply);
                        }
                    });

                    Pump.pump(sourceFile, connectionResult.result()).start();

                    sourceFile.endHandler(handler -> {
                        sourceFile.close();
                        client.close();
                    });
                });
            });
        };


        message.reply(new JsonObject()
                        .put("senderAddress", getLANAddress().getHostAddress())
                        .put("fileName", file.getName())
                        .put("fileSize", file.length())
                , reply);

    }

    public static InetAddress getLANAddress() {
        try {
            InetAddress candidateAddress = null;
            for (Enumeration networkInterfaces = NetworkInterface.getNetworkInterfaces(); networkInterfaces.hasMoreElements();) {
                NetworkInterface networkInterface = (NetworkInterface) networkInterfaces.nextElement();

                for (Enumeration inetAddresses = networkInterface.getInetAddresses(); inetAddresses.hasMoreElements();) {
                    InetAddress inetAddress = (InetAddress) inetAddresses.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        if (inetAddress.isSiteLocalAddress()) {
                            return inetAddress;
                        }
                        else if (candidateAddress == null) {
                            candidateAddress = inetAddress;
                        }
                    }
                }
            }
            if (candidateAddress != null) {
                return candidateAddress;
            }
            InetAddress jdkSuppliedAddress = InetAddress.getLocalHost();
            if (jdkSuppliedAddress == null) {
                throw new UnknownHostException("Unable to identify this host's IP address!");
            }
            return jdkSuppliedAddress;
        }
        catch (Exception e) {
            UnknownHostException unknownHostException = new UnknownHostException("Failed to determine LAN address: " + e);
            unknownHostException.initCause(e);
            System.err.println("Unable to determine host's LAN address!" + e);
            return null;
        }
    }

    @AfterClass
    public static void tearDownVertx(TestContext context) {
        //FileUtils.deleteFolder(webRootDirectory);
        //File targetFolder = new File(webRootDirectory);
        //targetFolder.mkdir();

        try {
            vertx.close();
        }
        catch(Exception e) {}
    }
}
