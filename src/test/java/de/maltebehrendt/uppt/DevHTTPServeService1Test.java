package de.maltebehrendt.uppt;

import de.maltebehrendt.uppt.util.FileUtils;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.file.FileSystem;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * Created by malte on 23.05.17.
 */
@RunWith(VertxUnitRunner.class)
public class DevHTTPServeService1Test {

    private static Vertx vertx = null;
    private static EventBus eventBus = null;
    private static FileSystem fileSystem = null;

    private static String configDirectory = "test" + File.separator + "devHTTPServeServiceTest" + File.separator + "config" + File.separator;

    private static String sourceFolderPath = "test" + File.separator + "devHTTPServeServiceTest" + File.separator + "public" + File.separator;;
    private static String destinationFolderPath = "test" + File.separator + "devHTTPServeServiceTest" + File.separator + "webroot" + File.separator;
    private static Integer webBridgePort = 8080;
    private static Integer waitingTimeInMs = 1000;

    @BeforeClass
    public static void setupLocalVertx(TestContext context) {
        Async setupResult = context.async(2);

        // delete the destination folder and recreate it for having a fresh start
        FileUtils.deleteFolder(destinationFolderPath);
        File targetFolder = new File(destinationFolderPath);
        targetFolder.mkdir();

        InstanceRunner.setConfigDirectory(configDirectory);
        vertx = InstanceRunner.vertx(new JsonArray().add("userService1.json").add("webBridgeService1.json").add("devHTTPServeService1.json"), true);
        eventBus = vertx.eventBus();
        fileSystem = vertx.fileSystem();

        MessageConsumer<JsonObject> consumer = eventBus.consumer("system.1.newInstance", message -> {
            // startup of a service complete
            if ("200".equalsIgnoreCase(message.headers().get("statusCode"))) {
                context.assertEquals("200", message.headers().get("statusCode"));
                context.assertNotNull(message.headers().get("correlationID"));
                context.assertNotNull(message.headers().get("origin"));

                setupResult.countDown();
            }
        });

        // wait until the initial copy is done and the watch service is up
        MessageConsumer<JsonObject> readyConsumer = eventBus.consumer("devHTTPServeService.1.watcherReady", message -> {
            setupResult.countDown();

            // check that the inital copy is accurrate (not in own test as it leads to side effects)
            File sourceDir = new File(sourceFolderPath);
            File targetDir = new File(destinationFolderPath);

            try {
                context.assertTrue(areDirectoriesEqual(sourceDir, targetDir), "Initial folder copies are equal.");
            } catch (Exception e) {
                context.assertTrue(false, "Initial folder copies are equal: " + e.getMessage());
            }
            setupResult.countDown();
        });


        setupResult.awaitSuccess();
        consumer.unregister();
        readyConsumer.unregister();
    }

    @Test
    public void testWebBridgeServingInitialFile(TestContext context) {
        Async testResult = context.async(1);

        // get the index.html
        HttpClientOptions clientOptions = new HttpClientOptions()
                .setTrustAll(true);
        HttpClient client = vertx.createHttpClient(clientOptions);
        client.getNow(webBridgePort, "localhost", "/public/index.html", responseHandler -> {
            context.assertEquals(200, responseHandler.statusCode(), "Get request was successful, status code 200");

            responseHandler.bodyHandler(body -> {
                String fileContent = body.toString();
                context.assertTrue(fileContent.length() > 0, "Non-empty file was returned");
                testResult.countDown();
            });

        });

        testResult.awaitSuccess();
    }

    @Test
    public void testNewFile(TestContext context) {
        Async testResult = context.async(4);

        File srcFile = new File(sourceFolderPath + File.separator + "index.html");
        File newFile = new File(sourceFolderPath + File.separator + "index.new.html");
        File targetFile = new File(destinationFolderPath + File.separator + "index.new.html");

        if(newFile.exists()) {
            newFile.delete();
        }
        if(targetFile.exists()) {
            targetFile.delete();
        }

        fileSystem.copy(srcFile.getAbsolutePath(), newFile.getAbsolutePath(), result -> {
            context.assertTrue(result.succeeded(), "Duplicated index.html for checking new file handling/detection");
            testResult.countDown();

            vertx.setTimer(waitingTimeInMs, handler -> {
                // check that the file is in the target directory
                context.assertTrue(targetFile.exists(), "New file was copied to target directory");
                context.assertTrue(srcFile.length() == targetFile.length(), "Copied file has the same size");
                testResult.countDown();

                // check that the WebBridge serves the new file
                HttpClientOptions clientOptions = new HttpClientOptions()
                        .setTrustAll(true);
                HttpClient client = vertx.createHttpClient(clientOptions);
                client.getNow(webBridgePort, "localhost", "/public/index.new.html", responseHandler -> {
                    context.assertEquals(200, responseHandler.statusCode(), "Get request was successful, status code 200");

                    responseHandler.bodyHandler(body -> {
                        String fileContent = body.toString();
                        context.assertTrue(fileContent.length() == newFile.length(), "New file was served");
                        testResult.countDown();

                        fileSystem.delete(newFile.getAbsolutePath(), delResult -> {
                            context.assertTrue(delResult.succeeded(), "New file was deleted again");
                            testResult.countDown();
                        });
                    });

                });
            });
        });


        testResult.awaitSuccess();
    }


    @Test
    public void testDeletedFile(TestContext context) {
        Async testResult = context.async(5);

        File srcFile = new File(sourceFolderPath + File.separator + "index.html");
        File newFile = new File(sourceFolderPath + File.separator + "index.del.html");
        File targetFile = new File(destinationFolderPath + File.separator + "index.del.html");

        if(newFile.exists()) {
            newFile.delete();
        }
        if(targetFile.exists()) {
            targetFile.delete();
        }

        fileSystem.copy(srcFile.getAbsolutePath(), newFile.getAbsolutePath(), result -> {
            context.assertTrue(result.succeeded(), "Duplicated index.html for checking del file handling/detection");
            testResult.countDown();

            vertx.setTimer(waitingTimeInMs, handler -> {
                // check that the file is in the target directory

                context.assertTrue(targetFile.exists(), "New file was copied to target directory");
                context.assertTrue(srcFile.length() == targetFile.length(), "Copied file has the same size");
                testResult.countDown();

                // delete file in source
                fileSystem.delete(newFile.getAbsolutePath(), delResult -> {
                    context.assertTrue(delResult.succeeded(), "New file was deleted again");
                    testResult.countDown();

                    vertx.setTimer(waitingTimeInMs, impactHandler -> {
                        context.assertFalse(targetFile.exists(), "Target file was deleted in target directory");
                        testResult.countDown();

                        // check that the WebBridge does not serve the deleted file
                        HttpClientOptions clientOptions = new HttpClientOptions()
                                .setTrustAll(true);
                        HttpClient client = vertx.createHttpClient(clientOptions);
                        client.getNow(webBridgePort, "localhost", "/public/index.del.html", responseHandler -> {
                            context.assertEquals(404, responseHandler.statusCode(), "Deleted file request caused a 404");
                            testResult.countDown();
                        });
                    });
                });
            });
        });

        testResult.awaitSuccess();
    }

    @Test
    public void testNewFolder(TestContext context) {
        Async testResult = context.async(6);

        File sourceFolder = new File(sourceFolderPath + File.separator + "data");
        File newFolder = new File(sourceFolderPath + File.separator + "data.dup");
        File targetFolder = new File(destinationFolderPath + File.separator + "data.dup");

        if(newFolder.exists()) {
            fileSystem.deleteRecursiveBlocking(newFolder.getAbsolutePath(), true);
        }
        if(targetFolder.exists()) {
            fileSystem.deleteRecursiveBlocking(targetFolder.getAbsolutePath(), true);
        }

        fileSystem.copyRecursive(sourceFolder.getAbsolutePath(), newFolder.getAbsolutePath(), true, result -> {
            context.assertTrue(result.succeeded(), "Duplicated data folder for checking new folder");
            testResult.countDown();

            vertx.setTimer(waitingTimeInMs, handler -> {
                // check that the new folder is in the target directory

                context.assertTrue(targetFolder.exists() && targetFolder.isDirectory(), "Folder has been duplicated into target folder");
                try {
                    context.assertTrue(areDirectoriesEqual(sourceFolder, targetFolder), "Folder has been duplicated correctly");
                } catch (Exception e) {
                    context.assertTrue(false, "Folder has been duplicated correctly");
                    return;
                }
                testResult.countDown();

                // check that files are served from the duplicated folder
                HttpClientOptions clientOptions = new HttpClientOptions()
                        .setTrustAll(true);
                HttpClient client = vertx.createHttpClient(clientOptions);
                client.getNow(webBridgePort, "localhost", "/public/data.dup/sample_success_response.json", responseHandler -> {
                    context.assertEquals(200, responseHandler.statusCode(), "File from new folder is being served");

                    responseHandler.bodyHandler(body -> {
                        String fileContent = body.toString();
                        context.assertTrue(fileContent.contains("successMessage"), "Correct file from new folder was served");
                        testResult.countDown();

                        // TODO: check that modifications (e.g. new files) in the duplicated folder are being observed
                        try {
                            FileWriter writer = new FileWriter(newFolder.getAbsolutePath() + File.separator + "new_response.json", true);
                            writer.write(fileContent);
                            writer.close();
                        } catch (IOException e) {
                            context.assertTrue(false, "New file has been writen into duplicated folder");
                            return;
                        }

                        vertx.setTimer(waitingTimeInMs, newHandler -> {
                           File targetFile = new File(targetFolder.getAbsolutePath() + File.separator + "new_response.json");
                            context.assertTrue(targetFile.exists(), "New file in duplicated folder has been duplicated");
                            testResult.countDown();

                            // delete folder in source directory
                            fileSystem.deleteRecursive(newFolder.getAbsolutePath(), true, delResult -> {
                                context.assertTrue(delResult.succeeded(), "Duplicated folder was deleted in source directory");
                                testResult.countDown();

                                vertx.setTimer(waitingTimeInMs, waitHandler -> {
                                    context.assertFalse(targetFolder.exists(), "Duplicated folder was deleted in target directory");
                                    testResult.countDown();
                                });
                            });
                        });
                    });
                });
            });
        });

        testResult.awaitSuccess();
    }

    @Test
    public void testDeletedFolder(TestContext context) {
        Async testResult = context.async(4);

        File sourceFolder = new File(sourceFolderPath + File.separator + "data");
        File newFolder = new File(sourceFolderPath + File.separator + "data.del");
        File targetFolder = new File(destinationFolderPath + File.separator + "data.del");

        if(newFolder.exists()) {
            fileSystem.deleteRecursiveBlocking(newFolder.getAbsolutePath(), true);
        }
        if(targetFolder.exists()) {
            fileSystem.deleteRecursiveBlocking(targetFolder.getAbsolutePath(), true);
        }

        fileSystem.copyRecursive(sourceFolder.getAbsolutePath(), newFolder.getAbsolutePath(), true, result -> {
            context.assertTrue(result.succeeded(), "Duplicated data folder for checking folder deletion");
            testResult.countDown();

            vertx.setTimer(waitingTimeInMs, handler -> {
                // check that the new folder is in the target directory

                context.assertTrue(targetFolder.exists() && targetFolder.isDirectory(), "Folder has been duplicated into target folder");
                try {
                    context.assertTrue(areDirectoriesEqual(newFolder, targetFolder), "Folder has been duplicated correctly");
                } catch (Exception e) {
                    context.assertTrue(false, "Folder has been duplicated correctly: " + e.getMessage());
                    return;
                }
                testResult.countDown();

                // delete folder in source directory
                fileSystem.deleteRecursive(newFolder.getAbsolutePath(), true, delResult -> {
                    context.assertTrue(delResult.succeeded(), "Duplicated folder was deleted in source directory");
                    testResult.countDown();

                    vertx.setTimer(waitingTimeInMs, waitHandler -> {
                        context.assertFalse(targetFolder.exists(), "Duplicated folder was deleted in target directory");
                        testResult.countDown();
                    });
                });
            });
        });

        testResult.awaitSuccess();
    }

    @Test
    public void testModifiedFile(TestContext context) {
        Async testResult = context.async(6);

        File srcFile = new File(sourceFolderPath + File.separator + "index.html");
        File newFile = new File(sourceFolderPath + File.separator + "index.mod.html");
        File targetFile = new File(destinationFolderPath + File.separator + "index.mod.html");

        if(newFile.exists()) {
            newFile.delete();
        }
        if(targetFile.exists()) {
            targetFile.delete();
        }

        fileSystem.copy(srcFile.getAbsolutePath(), newFile.getAbsolutePath(), result -> {
            context.assertTrue(result.succeeded(), "Duplicated index.html for checking file change handling/detection");
            testResult.countDown();

            vertx.setTimer(waitingTimeInMs, handler -> {
                // check that the file is in the target directory

                context.assertTrue(targetFile.exists(), "New file was copied to target directory");
                context.assertTrue(srcFile.length() == targetFile.length(), "Copied file has the same size");
                testResult.countDown();

                // check that the WebBridge serves the new file
                HttpClientOptions clientOptions = new HttpClientOptions()
                        .setTrustAll(true);
                HttpClient client = vertx.createHttpClient(clientOptions);
                client.getNow(webBridgePort, "localhost", "/public/index.mod.html", responseHandler -> {
                    context.assertEquals(200, responseHandler.statusCode(), "Get request was successful, status code 200");

                    responseHandler.bodyHandler(body -> {
                        String fileContent = body.toString();
                        context.assertTrue(fileContent.length() == srcFile.length(), "New file was served");
                        testResult.countDown();

                        // modify source file...
                        String newTitle = "<title>Shop - " + UUID.randomUUID().toString() + "</title>";
                        List<String> lines = new LinkedList<String>();
                        try(Stream<String> stream = Files.lines(newFile.toPath())) {
                            stream.forEach(line -> {
                                if(line.contains("<title>")) {
                                    lines.add(newTitle);
                                }
                                else {
                                    lines.add(line);
                                }
                            });
                        }
                        catch (IOException e) {
                            context.assertTrue(false, "Read mod file into memory");
                            return;
                        }
                        try {
                            Files.write(newFile.toPath(), lines);
                            testResult.countDown();
                        } catch (IOException e) {
                            context.assertTrue(false, "Wrote modified file into source directory");
                            return;
                        }

                        // check that new file version is being served...
                        vertx.setTimer(waitingTimeInMs, waitHandler -> {
                            client.getNow(webBridgePort, "localhost", "/public/index.mod.html", updatedResponseHandler -> {
                                context.assertEquals(200, updatedResponseHandler.statusCode(), "Get request for modified file was successful, status code 200");

                                updatedResponseHandler.bodyHandler(updatedBody -> {
                                    String modifiedFileContent = updatedBody.toString();
                                    context.assertTrue(modifiedFileContent.contains(newTitle), "File content has been updated");
                                    testResult.countDown();

                                    fileSystem.delete(newFile.getAbsolutePath(), delResult -> {
                                        context.assertTrue(delResult.succeeded(), "New file was deleted again");
                                        testResult.countDown();
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });

        testResult.awaitSuccess();
    }

    @AfterClass
    public static void tearDownVertx(TestContext context) {
        try {
            vertx.close();
        }
        catch(Exception e) {}
    }

    private static Boolean areDirectoriesEqual(File dir1, File dir2) throws Exception {
        if(!dir1.exists()
                || !dir1.isDirectory()
                || !dir2.exists()
                || !dir2.isDirectory()) {
            System.err.println("One of the directories does not exist!");
            return false;
        }

        Files.walkFileTree(dir1.toPath(), new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attr) throws IOException {
                FileVisitResult result = super.preVisitDirectory(dir, attr);

                Path relativePath = dir1.toPath().relativize(dir);
                File otherDir = dir2.toPath().resolve(relativePath).toFile();

                if(!otherDir.exists()) {
                    throw new IOException("Folder " + relativePath + " missing!");
                }
                if(!Arrays.equals(otherDir.list(), dir.toFile().list())) {
                    throw new IOException("Folder lists are different for: " + relativePath);
                }

                return result;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attr) throws IOException {
                FileVisitResult result = super.visitFile(file, attr);

                // only compare file size here..
                Path relativePath = dir1.toPath().relativize(file);
                File otherFile = dir2.toPath().resolve(relativePath).toFile();

                if(!otherFile.exists()) {
                    throw new IOException("File " + relativePath + " missing!");
                }
                if(otherFile.length() != attr.size()) {
                    throw new IOException("File sizes " + relativePath + " not equal!");
                }

                return result;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                super.visitFileFailed(file, exc);
                throw exc;
            }
        });

        return true;
    }
}
