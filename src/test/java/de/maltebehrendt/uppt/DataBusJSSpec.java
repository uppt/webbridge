package de.maltebehrendt.uppt;

import de.maltebehrendt.uppt.testing.JasmineTestRunner;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by malte on 06.02.17.
 */
@RunWith(VertxUnitRunner.class)
@de.helwich.junit.JasmineTest(
        src =  { "webbridge/1.0/libs/uppt-databus" },
        test = { "webbridge/1.0/libs/uppt-databus-spec" }
)
public class DataBusJSSpec extends JasmineTestRunner {

    private static Vertx vertx = null;
    private static EventBus eventBus = null;

    private static String configDirectory = "test" + File.separator + "dataBusJSSpecTest" + File.separator + "config" + File.separator;
    private static String versionPrefix = "/webbridge/1.0";
    private static String upptLocation = "http://localhost:8080";
    private static String upptRouterAddress = "in.test.1.router";


    @BeforeClass
    public static void setupLocalVertx(TestContext context) {
        Async setupResult = context.async(1);

        InstanceRunner.setConfigDirectory(configDirectory);
        vertx = InstanceRunner.vertx(new JsonArray().add("userService1.json").add("webBridgeService1.json").add("solutionService1.json"), true);
        eventBus = vertx.eventBus();

        MessageConsumer<JsonObject> consumer = eventBus.consumer("system.1.newInstance", message -> {
            // startup of a service complete
            if ("200".equalsIgnoreCase(message.headers().get("statusCode"))) {
                context.assertEquals("200", message.headers().get("statusCode"));
                context.assertNotNull(message.headers().get("correlationID"));
                context.assertNotNull(message.headers().get("origin"));

                setupResult.countDown();
            }
        });

        setupResult.awaitSuccess();
        consumer.unregister();
    }

    @Test
    public void testUpptJS(TestContext context) {
        Async testResult = context.async(2);

        vertx.executeBlocking(future -> {
            // setup valid user account
            createNewUser(new JsonArray().add("tester"), future);
        }, createUserResult -> {
            context.assertTrue(createUserResult.succeeded(), "New test user was created");
            JsonObject newUserInfo = (JsonObject) createUserResult.result();
            context.assertTrue(newUserInfo != null && !newUserInfo.isEmpty(), "New user information have been returned");
            testResult.countDown();

            // setup the test environment on client side (remember to set the router eventbus address!)
            setupJasmineTestRunner(vertx, versionPrefix,  upptLocation, upptRouterAddress, new JsonObject()
                    .put("email", newUserInfo.getString("email"))
                    .put("password", newUserInfo.getString("password"))
            );
            // run the Jasmine specification test
            vertx.executeBlocking(future -> {
                runJasmineTests(context, testResult);
            }, result -> {});

        });

        testResult.awaitSuccess();
    }

    public void createNewUser(JsonArray roles, Future<Object> future) {
        int randomNumber = ThreadLocalRandom.current().nextInt(1, 5001);

        JsonObject newEMailUser = new JsonObject()
                .put("email", "test" + randomNumber +"@test.de")
                .put("passwordKeyFactory", "PBKDF2WithHmacSHA512")
                .put("passwordSalt", "testSaltToping")
                .put("passwordIterations", 10)
                .put("passwordKeyLength", 256);

        if(roles != null) {
            newEMailUser.put("roles", roles);
        }
        else {
            newEMailUser.put("roles", new JsonArray().add("tester").add("superTester"));
        }

        String password = "superSecret12" + randomNumber;
        String passwordHash = hashPassword(password,
                newEMailUser.getString("passwordKeyFactory"),
                newEMailUser.getString("passwordSalt"),
                newEMailUser.getInteger("passwordIterations"),
                newEMailUser.getInteger("passwordKeyLength"));
        newEMailUser.put("passwordHash", passwordHash);
        newEMailUser.put("password", password);

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "858518121")
                .addHeader("origin", "WebBridgeService1Test");

        eventBus.send("users.1.newEMailUser", newEMailUser, options, reply -> {
            if(reply.failed() || !"200".equals(reply.result().headers().get("statusCode"))) {
                future.fail(reply.cause());
            }
            else {
                future.complete(newEMailUser);
            }
        });
    }

    public void deleteUser(String id, Future<Object> future) {
        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "858518121")
                .addHeader("origin", "WebBridgeService1Test");

        eventBus.send("users.1.userBecameObsolete",new JsonObject().put("id", id), options, delReply -> {
            if(delReply.failed() || !"200".equals(delReply.result().headers().get("statusCode"))) {
                future.fail(delReply.cause());
            }
            else {
                future.complete(id);
            }
        });
    }


    @AfterClass
    public static void tearDownVertx(TestContext context) {
        try {
            vertx.close();
        }
        catch(Exception e) {}
    }


}
