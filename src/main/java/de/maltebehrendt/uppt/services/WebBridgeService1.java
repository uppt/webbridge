package de.maltebehrendt.uppt.services;


import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import de.maltebehrendt.uppt.annotation.Customer;
import de.maltebehrendt.uppt.annotation.Payload;
import de.maltebehrendt.uppt.annotation.Processor;
import de.maltebehrendt.uppt.authorization.User;
import de.maltebehrendt.uppt.enums.DataType;
import de.maltebehrendt.uppt.events.Message;
import de.maltebehrendt.uppt.util.ZipUtils;
import io.vertx.core.Future;
import io.vertx.core.http.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.net.JksOptions;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.ext.web.handler.sockjs.SockJSHandlerOptions;
import io.vertx.ext.web.sstore.LocalSessionStore;
import org.apache.commons.codec.binary.Base64;
import org.joda.time.DateTime;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by malte on 31.01.17.
 *
 * Todo: outsource this service into it's own package (when the pods are being dealt with)
 */
public class WebBridgeService1 extends AbstractService {

    private ConcurrentHashMap<String, JsonObject> inboundWhitelist = null;
    private ConcurrentHashMap<String, JsonObject> outboundWhitelist = null;
    private ConcurrentHashMap<String, String> availableBridges = null;
    private ConcurrentHashMap<String, JsonObject> publicResourcesFileMap = null;
    private AtomicInteger publicResourcesFileRetrievalQueueCount = null;
    private Cache<String, User> tokenCache = null;
    private HttpServer httpServer = null;
    private boolean isBridgeRunning = false;

    @Override
    public void prepare(Future<Object> prepareFuture) {
        isBridgeRunning = false;
        inboundWhitelist = new ConcurrentHashMap<>();
        inboundWhitelist.put("in.sessions.1.setup", new JsonObject());
        outboundWhitelist = new ConcurrentHashMap<>();
        availableBridges = new ConcurrentHashMap<>();
        publicResourcesFileMap = new ConcurrentHashMap<>();
        publicResourcesFileRetrievalQueueCount = new AtomicInteger(0);
        tokenCache = CacheBuilder.newBuilder()
                .expireAfterAccess(1, TimeUnit.HOURS)
                .build();

        // clear WebRoot folder, if existent. Otherwise create it
        File webRoot = new File(config().getJsonObject("publicHTTPServer").getString("publicResourcesFolderPath"));
        if(webRoot.exists() && webRoot.isDirectory()) {
            try {
                Files.walkFileTree(webRoot.toPath(), new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                        try {
                            Files.delete(file);
                        }
                        catch (IOException e) {
                            logger.error("Failed to empty web root directory for public files! ", e);
                        }
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException ioException) {
                        try {
                            Files.delete(dir);
                        }
                        catch (IOException e) {
                            logger.error("Failed to empty web root directory for public files! ", e);
                        }
                        return FileVisitResult.CONTINUE;
                    }

                });
                webRoot.mkdir();
            } catch (IOException e) {
                logger.error("Failed to empty web root directory for public files! ", e);
                prepareFuture.fail("Failed to empty web root directory for public files!");
            }
        }
        else {
            webRoot.mkdir();
        }

        // register a processor for getting bridges before start consuming, so that they are available as early as possible
        JsonObject missingBridgeProcessorRecord = new JsonObject()
                .put("description", "Bridges an eventBus address to the outside world")
                .put("domain", "webBridge")
                .put("version", "1")
                .put("type", "missingBridge")
                .put("category", "Processor")
                .put("provides", new JsonArray())
                .put("requires", new JsonArray()
                        .add(new JsonObject()
                                .put("key", "domain")
                                .put("type", "STRING")
                                .put("description", "Address domain"))
                        .add(new JsonObject()
                                .put("key", "version")
                                .put("type", "STRING")
                                .put("description", "Address version"))
                        .add(new JsonObject()
                                .put("key", "type")
                                .put("type", "STRING")
                                .put("description", "Address type"))
                        .add(new JsonObject()
                                .put("key", "isInbound")
                                .put("type", "BOOLEAN")
                                .put("description", "Bridging direction, messages from outside into the system are allowed"))
                        .add(new JsonObject()
                                .put("key", "isOutbound")
                                .put("type", "BOOLEAN")
                                .put("description", "Bridging direction, messages from system to the outside are allowed"))
                        .add(new JsonObject()
                                .put("key", "authorities")
                                .put("type", "JSONObject")
                                .put("description", "Contains roles, users or sessions arrays for specifying allowed access. Empty for allowing unauthorized access."))
                );

        try {
            Method method = this.getClass().getMethod("processMissingBridge", Message.class);
            registerProcessor(missingBridgeProcessorRecord, method, this);
            publish("webBridge", "1", "newWebBridgingInstance", new JsonObject());
        } catch (NoSuchMethodException e) {
            logger.error("Method processMissingBridge not found!", e);
            prepareFuture.fail("processMissingBridge method not found!");
            return;
        }

        if(config().getBoolean("isPublicHTTPServerEnabled")) {
            // register a processor for receiving public files from new service instances
            // using an official processor would be registered too late (after prepare) for receiving files from services
            // that are starting up at the same time
            JsonObject processorRecord = new JsonObject()
                    .put("description", "Retrieves public resources from a service and unpacks it")
                    .put("domain", "publicResources")
                    .put("version", "1")
                    .put("type", "newResourceZip")
                    .put("category", "Processor")
                    .put("provides", new JsonArray())
                    .put("requires", new JsonArray());

            try {
                Method method = this.getClass().getMethod("processNewPublicResourceZip", Message.class);
                registerProcessor(processorRecord, method, this);
            } catch (NoSuchMethodException e) {
                logger.error("Public HTTP Server failure: processNewPublicResourceZip method not found", e);
                prepareFuture.fail("processNewPublicResourceZip method not found!");
                return;
            }
            // request public file zips from running services (resend publicResources.1.newInstance)
            publish("publicResources", "1", "newInstance", new JsonObject());

            final Long[] periodicID = {0L};

            vertx.setTimer(config().getJsonObject("publicHTTPServer").getLong("initialFileRequestWaitingTime"), handler -> {
                if(publicResourcesFileRetrievalQueueCount.get() == 0) {
                    startServer(prepareFuture);
                }
                else {
                    periodicID[0] = vertx.setPeriodic(500, check -> {
                        if(publicResourcesFileRetrievalQueueCount.get() == 0) {
                            startServer(prepareFuture);
                            vertx.cancelTimer(periodicID[0]);
                        }
                    });
                }
            });
        }
        else {
            startServer(prepareFuture);
        }
    }

    @Override
    public void startConsuming() {
        // note: server already started in prepare! Might be dangerous!
    }

    private void startServer(Future<Object> startFuture) {
        // loading configuration and set defaults, if missing
        loadConfig();

        // request bridges (routers) from running services (resend webBridge.1.missingBridge)

        Router router = Router.router(vertx);

        // Preparing user authorization
        router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));


        // CORS setup
        String allowedOriginPattern = config().getJsonObject("CORS").getString("allowedOriginPattern");
        if(allowedOriginPattern != null && !allowedOriginPattern.isEmpty()) {
            router.route().handler(CorsHandler
                    .create(allowedOriginPattern)
                    .allowedMethod(HttpMethod.GET)
                    .allowedMethod(HttpMethod.POST)
                    .allowedMethod(HttpMethod.OPTIONS)
                    .allowedHeader("Content-Type")
                    .allowedHeader("Authorization")
                    .allowedHeader("www-authenticate")
                    .allowedHeader("Access-Control-Request-Method")
                    .allowedHeader("Access-Control-Allow-Credentials")
                    .allowedHeader("Access-Control-Allow-Origin")
                    .allowedHeader("Access-Control-Allow-Headers")
                    .allowedHeader("Cookie")
                    .allowedHeader("Set-Cookie")
                    .allowedHeader("Content-Length")
                    .allowedHeader("XSessionToken")
            );
            logger.warn("SECURITY: configuring CORS allowed origin pattern to allow (incl. get and post): " + allowedOriginPattern);
        }

        // SockJS/WebSocket setup
        if(config().getBoolean("isWebSocketBridgeEnabled")) {
            SockJSHandlerOptions sockJSHandlerOptions = new SockJSHandlerOptions().setHeartbeatInterval(config().getJsonObject("webSocketBridge").getInteger("heartbeatInterval"));
            SockJSHandler sockJSHandler = SockJSHandler.create(vertx, sockJSHandlerOptions);

            // Note I'm doing something very dangerous here: I DISABLE  Vert.x own SECURITY/blacklist and allow any address with the prefix "in"/"out"!
            BridgeOptions bridgeOptions = new BridgeOptions().setReplyTimeout(config().getJsonObject("webSocketBridge").getLong("replyTimeout"));
            bridgeOptions.addInboundPermitted(new PermittedOptions().setAddressRegex("in\\..+"));
            bridgeOptions.addOutboundPermitted(new PermittedOptions().setAddressRegex("out\\..+"));

            sockJSHandler.bridge(bridgeOptions, bridgeEvent -> {
                String address = null;
                // Here I'm trying to ADD SECURITY on-the-fly again...
                switch(bridgeEvent.type()) {
                    case SEND:
                        // This event will occur when a message is attempted to be sent from the client to the server
                    case PUBLISH:
                        // This event will occur when a message is attempted to be published from the client to the server
                        address = bridgeEvent.getRawMessage().getString("address");
                        if(address == null || !inboundWhitelist.containsKey(address)) {
                            bridgeEvent.complete(false);
                            return;
                        }
                        JsonObject rawMessage = bridgeEvent.getRawMessage();
                        JsonObject inAuthorities = inboundWhitelist.get(address);

                        if(inAuthorities == null) {
                            // address is not on whitelist
                            String correlationID = rawMessage.containsKey("headers")?rawMessage.getJsonObject("headers").getString("correlationID"):"Unknown correlationID";
                            logger.warn("[" + correlationID + "] Unauthorized access attempt on " + address + " from " + bridgeEvent.socket().remoteAddress().host());
                            bridgeEvent.complete(false);
                            return;
                        }

                        // check authentication (incl. workaround for devices not supporting cookies)
                        User user = (User) bridgeEvent.socket().webUser();

                        // workarounds for issues with cookies (testing and mobile devices/Android 5)
                        if(user == null && rawMessage.containsKey("headers") && rawMessage.getJsonObject("headers").containsKey("xSessionToken")) {
                            user = tokenCache.getIfPresent(rawMessage.getJsonObject("headers").getString("xSessionToken"));
                            bridgeEvent.socket().webSession().put("user", user);
                        }
                        if(user == null && rawMessage.containsKey("body") && rawMessage.getJsonObject("body").containsKey("xSessionToken")) {
                            user = tokenCache.getIfPresent(rawMessage.getJsonObject("body").getString("xSessionToken"));
                            bridgeEvent.socket().webSession().put("user", user);
                        }

                        // authorization required but user not available
                        if(!inAuthorities.isEmpty() && user == null) {
                            String correlationID = rawMessage.containsKey("headers")?rawMessage.getJsonObject("headers").getString("correlationID"):"Unknown correlationID";
                            logger.warn("[" + correlationID + "] Unauthenticated access attempt on " + address + " from " + bridgeEvent.socket().remoteAddress().host());
                            bridgeEvent.complete(false);
                            return;
                        }
                        if(user != null) {
                            // check that the remote address matches the token
                            if (user.sessionSource() == null || !bridgeEvent.socket().remoteAddress().host().equals(user.sessionSource())) {
                                String correlationID = rawMessage.containsKey("headers")?rawMessage.getJsonObject("headers").getString("correlationID"):"Unknown correlationID";
                                logger.warn("[" + correlationID + "] Token/Remote address mismatch during access attempt on " + address + " by " + user.id() + ". Was: " + user.sessionSource() + " Is: " + bridgeEvent.socket().remoteAddress().host());
                                bridgeEvent.complete(false);
                                return;
                            }

                            // check if user is allowed to access the address
                            if(!inAuthorities.isEmpty()) {
                                boolean isAuthorized = false;

                                if(inAuthorities.containsKey("roles")) {
                                    JsonArray roles = user.roles();
                                    JsonArray authorizedRoles = inAuthorities.getJsonArray("roles");
                                    for (int i = 0; i < roles.size(); i++) {
                                        if (authorizedRoles.contains(roles.getString(i))) {
                                            isAuthorized = true;
                                            break;
                                        }
                                    }
                                }
                                if(!isAuthorized && inAuthorities.containsKey("sessions")) {
                                    JsonArray authorizedSessions = inAuthorities.getJsonArray("sessions");
                                    if (authorizedSessions.contains(user.sessionToken())) {
                                        isAuthorized = true;
                                    }
                                }
                                if(!isAuthorized && inAuthorities.containsKey("users")) {
                                    JsonArray authorizedUsers = inAuthorities.getJsonArray("users");
                                    if (authorizedUsers.contains(user.id())) {
                                        isAuthorized = true;
                                    }
                                }

                                if(!isAuthorized) {
                                    String correlationID = rawMessage.containsKey("headers")?rawMessage.getJsonObject("headers").getString("correlationID"):"Unknown correlationID";
                                    logger.warn("[" + correlationID + "] Unauthorized access attempt on " + address + " by " + user.id() + " from " + bridgeEvent.socket().remoteAddress().host());
                                    bridgeEvent.complete(false);
                                    return;
                                }
                            }

                            if("in.sessions.1.setup".equals(address)) {
                                // information required for processing the setup...
                                rawMessage.getJsonObject("body")
                                        .put("sessionSource", bridgeEvent.socket().remoteAddress().host())
                                        .put("sessionToken", user.sessionToken())
                                        .put("sessionStartTimestamp", new DateTime().getMillis());
                            }

                            rawMessage.put("headers", new JsonObject()
                                    .put("origin", "webUser")
                                    .put("userID", user.id())
                                    .put("userRoles", user.roles().encode())
                                    .put("sessionAddress", user.sessionAddress())
                            );
                        }
                        else {
                            rawMessage.put("headers", new JsonObject()
                                    .put("origin", "webUser")
                                    .put("userID", "")
                                    .put("userRoles", "[]")
                                    .put("sessionAddress", "")
                            );
                        }

                        rawMessage.getJsonObject("body").remove("xSessionToken");
                        bridgeEvent.setRawMessage(rawMessage);
                        bridgeEvent.complete(true);
                        break;
                    case RECEIVE:
                        // This event will occur when a message is attempted to be delivered from the server to the client
                        // basically only checking if address is (still) on the whitelist or a response, more fine-grained checking is done at the
                        // register event
                        address = bridgeEvent.getRawMessage().getString("address");
                        String type = bridgeEvent.getRawMessage().getString("type");

                        if(address == null || (!outboundWhitelist.containsKey(address) && !"rec".equals(type))) {
                            bridgeEvent.complete(false);
                            return;
                        }
                        bridgeEvent.complete(true);
                        break;
                    case REGISTER:
                        // This event will occur when a client attempts to register a handler, make sure
                        // the user is allowed to access this address...
                        address = bridgeEvent.getRawMessage().getString("address");
                        if(address == null || (!outboundWhitelist.containsKey(address))) {
                            String correlationID = bridgeEvent.getRawMessage().containsKey("headers")?bridgeEvent.getRawMessage().getJsonObject("headers").getString("correlationID"):"Unknown correlationID";
                            logger.warn("[" + correlationID + "] Unauthenticated attempt to register a handler for non-whitelisted " + address + " from " + bridgeEvent.socket().remoteAddress().host());
                            bridgeEvent.complete(false);
                            return;
                        }
                        // we don't use the workaround here as we assume a message has been send before (to in.sessions.1.setup)
                        User registerUser = (User) bridgeEvent.socket().webUser();
                        JsonObject registerMessage = bridgeEvent.getRawMessage();
                        JsonObject outAuthorities = outboundWhitelist.get(address);

                        // workarounds for issues with cookies (testing and mobile devices/Android 5)
                        if(registerUser == null && registerMessage.containsKey("headers") && registerMessage.getJsonObject("headers").containsKey("xSessionToken")) {
                            registerUser = tokenCache.getIfPresent(registerMessage.getJsonObject("headers").getString("xSessionToken"));
                            bridgeEvent.socket().webSession().put("user", registerUser);
                        }
                        if(registerUser == null && registerMessage.containsKey("body") && registerMessage.getJsonObject("body").containsKey("xSessionToken")) {
                            registerUser = tokenCache.getIfPresent(registerMessage.getJsonObject("body").getString("xSessionToken"));
                            bridgeEvent.socket().webSession().put("user", registerUser);
                        }
                        if(!outAuthorities.isEmpty() && registerUser == null) {
                            String correlationID = registerMessage.containsKey("headers")?registerMessage.getJsonObject("headers").getString("correlationID"):"Unknown correlationID";
                            logger.warn("[" + correlationID + "] Unauthenticated attempt to register a handler for " + address + " from " + bridgeEvent.socket().remoteAddress().host());
                            bridgeEvent.complete(false);
                            return;
                        }
                        if(registerUser != null) {
                            // check that the remote address matches the token
                            if (registerUser.sessionSource() == null || !bridgeEvent.socket().remoteAddress().host().equals(registerUser.sessionSource())) {
                                String correlationID = registerMessage.containsKey("headers") ? registerMessage.getJsonObject("headers").getString("correlationID") : "Unknown correlationID";
                                logger.warn("[" + correlationID + "] Token/Remote address mismatch during registration attempt on " + address + " by " + registerUser.id() + ". Was: " + registerUser.sessionSource() + " Is: " + bridgeEvent.socket().remoteAddress().host());
                                bridgeEvent.complete(false);
                                return;
                            }

                            // check if user is allowed to access the address
                            if (!outAuthorities.isEmpty()) {
                                boolean isAuthorized = false;

                                if (outAuthorities.containsKey("roles")) {
                                    JsonArray roles = registerUser.roles();
                                    JsonArray authorizedRoles = outAuthorities.getJsonArray("roles");
                                    for (int i = 0; i < roles.size(); i++) {
                                        if (authorizedRoles.contains(roles.getString(i))) {
                                            isAuthorized = true;
                                            break;
                                        }
                                    }
                                }
                                if (!isAuthorized && outAuthorities.containsKey("sessions")) {
                                    JsonArray authorizedSessions = outAuthorities.getJsonArray("sessions");
                                    if (authorizedSessions.contains(registerUser.sessionToken())) {
                                        isAuthorized = true;
                                    }
                                }
                                if (!isAuthorized && outAuthorities.containsKey("users")) {
                                    JsonArray authorizedUsers = outAuthorities.getJsonArray("users");
                                    if (authorizedUsers.contains(registerUser.id())) {
                                        isAuthorized = true;
                                    }
                                }

                                if (!isAuthorized) {
                                    String correlationID = registerMessage.containsKey("headers") ? registerMessage.getJsonObject("headers").getString("correlationID") : "Unknown correlationID";
                                    logger.warn("[" + correlationID + "] Unauthorized registration attempt on " + address + " by " + registerUser.id() + " from " + bridgeEvent.socket().remoteAddress().host());
                                    bridgeEvent.complete(false);
                                    return;
                                }
                            }
                        }
                        bridgeEvent.complete(true);
                        break;
                    case UNREGISTER:
                        // This event will occur when a client attempts to unregister a handler
                        bridgeEvent.complete(true);
                        break;
                    case SOCKET_CREATED:
                    case SOCKET_CLOSED:
                        User webUser = (User) bridgeEvent.socket().webUser();
                        if(webUser != null && webUser.sessionToken() != null) {
                            publish("sessions", "1", "end", new JsonObject()
                                    .put("userID",webUser.id())
                                    .put("sessionToken",webUser.sessionToken())
                                    .put("sessionAddress",webUser.sessionAddress())
                                    .put("sessionSource",webUser.sessionSource())
                            );
                        }
                        bridgeEvent.complete(true);
                        break;
                }
            });

            // serve websocket connections from the eventbus path
            router.route("/eventbus/*").handler(sockJSHandler);
        }

        // here a ticket-based authorization method will be used...
        // note: the BodyHandler is behind the SockJS creation due to a bug: https://github.com/vert-x3/vertx-web/issues/775
        if(config().getBoolean("isEMailBasedLoginEnabled")) {
            router.route().handler(BodyHandler.create());
            router.post("/email/login").handler(loginContext -> {
                authenticateEMailUser(loginContext);
            });
        }

        HttpServerOptions httpServerOptions = new HttpServerOptions();

        // serve public files from the webroot-folder
        if(config().getBoolean("isPublicHTTPServerEnabled")) {
            router.route("/public/*").handler(StaticHandler.create(config().getJsonObject("publicHTTPServer").getString("publicResourcesFolderPath")));

            if (config().getJsonObject("publicHTTPServer").getBoolean("isHTTP2Enabled")) {
                if (!config().getBoolean("isSSLEnabled")) {
                    logger.fatal("Can not enable HTTP2 H2 without enabled SSL! Falling back to HTTP 1.1 only...");
                }
                httpServerOptions.setUseAlpn(true);
                httpServerOptions.setIdleTimeout(config().getJsonObject("publicHTTPServer").getInteger("idleTimeoutInSeconds"));
            }
        }

        // setup SSL
        if(config().getBoolean("isSSLEnabled")) {
            httpServerOptions
                    .setKeyStoreOptions(new JksOptions()
                            .setPath(config().getJsonObject("ssl").getString("keystorePath"))
                            .setPassword(config().getJsonObject("ssl").getString("keystorePassword")))
                    .setSsl(true)
                    .setClientAuth(ClientAuth.NONE);
        }
        else {
            httpServerOptions.setSsl(false);
            logger.warn("SECURITY: SSL is disabled. THIS IS PROBABLY A SEVERE RISK (especially regarding email-based logins)!");
        }

        // start Server
        Integer port = config().getInteger("port");
        vertx.createHttpServer(httpServerOptions).requestHandler(router::accept).listen(port, result -> {
            if(result.failed()) {
                logger.fatal("Failed to start HTTP Server!" , result.cause());
                startFuture.fail(result.cause());
            }
            else {
                logger.info("HTTP server started on port " + port);
                startFuture.complete();
            }
        });
    }

    // is registered manually so it's available before start consuming
    public void processMissingBridge(Message message) {
        JsonObject bridgingRequest= message.getBodyAsJsonObject();
        String domain = bridgingRequest.getString("domain");
        String version = bridgingRequest.getString("version");
        String type = bridgingRequest.getString("type");
        boolean isInbound = bridgingRequest.getBoolean("isInbound");
        boolean isOutbound = bridgingRequest.getBoolean("isOutbound");
        JsonObject authorities = bridgingRequest.getJsonObject("authorities");

        if(authorities != null && !authorities.isEmpty() && (!authorities.containsKey("roles") && !authorities.containsKey("sessions") && !authorities.containsKey("users"))) {
            logger.warn("[" + message.correlationID() + "] " + message.origin() + ": dropping missingBridge event due to erroneous payload: authorities");
            message.reply(400, "Specify access rights via roles, sessions or userID array in authorities!");
            return;
        }
        if(authorities == null) {
            authorities = new JsonObject();
        }

        if(domain == null || domain.isEmpty()) {
            logger.warn("[" + message.correlationID() + "] " + message.origin() + ": dropping missingBridge event due to erroneous payload: address");
            message.reply(400, "Address (domain) missing'");
            return;
        }
        if(version == null || version.isEmpty()) {
            logger.warn("[" + message.correlationID() + "] " + message.origin() + ": dropping missingBridge event due to erroneous payload: address");
            message.reply(400, "version (domain) missing");
            return;
        }
        if(type == null || type.isEmpty()) {
            logger.warn("[" + message.correlationID() + "] " + message.origin() + ": dropping missingBridge event due to erroneous payload: address");
            message.reply(400, "Address (domain) missing");
            return;
        }
        String address = domain + "." + version + "." + type;

        if(availableBridges.containsKey(address)) {
            // bridge is already available
            message.reply(200);
            return;
        }


        if(isInbound) {
            if(!address.startsWith("in.")) {
                logger.warn("[" + message.correlationID() + "] " + message.origin() + ": dropping missingBridge event due to erroneous payload: address must start with 'in.'");
                message.reply(400, "Address (domain) must start with 'in.'");
                return;
            }
            inboundWhitelist.put(address, authorities);
            availableBridges.put(address, address);
        }
        if(isOutbound) {
            if(!address.startsWith("out.")) {
                logger.warn("[" + message.correlationID() + "] " + message.origin() + ": dropping missingBridge event due to erroneous payload: address must start with 'out.'");
                message.reply(400, "Address (domain) must start with 'out.'");
                return;
            }
            outboundWhitelist.put(address, authorities);
            availableBridges.put(address, address);
        }

        if(authorities.isEmpty()) {
            logger.warn("[" + message.correlationID() + "] "
                    + message.origin() + " is ALLOWING UNAUTHENTICATED ACCESS to " + address);
        }

        logger.info("[" + message.correlationID() + "] "
                + message.origin() + " created bridge "
                + bridgingRequest.getString("domain") + "."
                + bridgingRequest.getString("version") + "."
                + bridgingRequest.getString("type")
                + " in direction inbound " + bridgingRequest.getBoolean("isInbound") + " and outbound " + bridgingRequest.getBoolean("isOutbound") + " for authorities: "
                + bridgingRequest.getJsonObject("authorities").toString()
        );

        message.reply(200);
    }

    // is registered manually as processor
    public void processNewPublicResourceZip(Message message) {
        String origin = message.origin();

        JsonObject payload = message.getBodyAsJsonObject()
                .put("origin", origin)
                .put("correlationID", message.correlationID());

        vertx.executeBlocking(future -> handleNewPublicResourceZip(payload, future), result -> {
            if(result.failed()) {
                String errorMessage = "Failed to retrieve/unpack new public resource zip " + payload.getString("fileName") + " from " + origin;
                logger.error(errorMessage, result.cause());
                message.reply(500, errorMessage);
            }
            else {
                logger.info("Unpacked public resource zip " + payload.getString("fileName") + " from " + origin);
                message.reply(200);
            }
        });
    }

    private void handleNewPublicResourceZip(JsonObject resourceInfo, Future<Object> future) {
        publicResourcesFileRetrievalQueueCount.getAndAdd(1);

        String fileName = resourceInfo.getString("fileName");
        String origin = resourceInfo.getString("origin");
        String serviceDomain = resourceInfo.getString("serviceDomain");
        String serviceType = resourceInfo.getString("serviceType");
        String serviceVersion = resourceInfo.getString("serviceVersion");

        String serviceFileID = origin + "_" + serviceDomain + serviceVersion + serviceType + "_" + fileName;

        if(publicResourcesFileMap.contains(serviceFileID)) {
            // note: this might lead to missed files, if a error occurrs in the following. At least until a instance of the providing service publishes his public resources again
            publicResourcesFileRetrievalQueueCount.getAndAdd(-1);
            return;
        }
        else {
            publicResourcesFileMap.put(serviceFileID, new JsonObject());
        }

        String relativeExtractionPath = resourceInfo.getString("relativeExtractionPath");
        String correlationID = resourceInfo.getString("correlationID");

        if(fileName == null || fileName.isEmpty()) {
            future.fail("[" + correlationID + "] Can not handle newPublicResourceZip as no file name was provided. Origin: " + origin);
            publicResourcesFileMap.remove(serviceFileID);
            publicResourcesFileRetrievalQueueCount.getAndAdd(-1);
            return;
        }
        if(!fileName.endsWith(".zip")) {
            logger.warn("[" + correlationID + "] Only zip files are supported as source for public resources. " + origin + " tried to provide: " + fileName);
            future.fail("[" + correlationID + "] Only zip files are supported. " + origin + " provided: " + fileName);
            publicResourcesFileMap.remove(serviceFileID);
            publicResourcesFileRetrievalQueueCount.getAndAdd(-1);
            return;
        }
        if(relativeExtractionPath == null || relativeExtractionPath.isEmpty() || relativeExtractionPath.contains("..") || relativeExtractionPath.matches("[:\"'*?<>|]+")) {
            future.fail("[" + correlationID + "] Can not handle newPublicResourceZip as no valid relative extraction path was provided. Path: " + relativeExtractionPath + " Origin: " + origin);
            publicResourcesFileMap.remove(serviceFileID);
            publicResourcesFileRetrievalQueueCount.getAndAdd(-1);
            return;
        }
        if(serviceDomain == null || serviceDomain.isEmpty()) {
            future.fail("[" + correlationID + "] Can not handle newPublicResourceZip as no serviceDomain for requesting the file was provided. File name: " + fileName + " Origin: " + origin);
            publicResourcesFileMap.remove(serviceFileID);
            publicResourcesFileRetrievalQueueCount.getAndAdd(-1);
            return;
        }
        if(serviceType == null || serviceType.isEmpty()) {
            future.fail("[" + correlationID + "] Can not handle newPublicResourceZip as no serviceType for requesting the file was provided. File name: " + fileName + " Origin: " + origin);
            publicResourcesFileMap.remove(serviceFileID);
            publicResourcesFileRetrievalQueueCount.getAndAdd(-1);
            return;
        }
        if(serviceVersion == null || serviceVersion.isEmpty()) {
            future.fail("[" + correlationID + "] Can not handle newPublicResourceZip as no serviceVersion for requesting the file was provided. File name: " + fileName + " Origin: " + origin);
            publicResourcesFileMap.remove(serviceFileID);
            publicResourcesFileRetrievalQueueCount.getAndAdd(-1);
            return;
        }

        send(serviceDomain, serviceVersion, serviceType, 100, correlationID, new JsonObject().put("fileName", fileName), reply -> {
            if(reply.failed() || reply.statusCode() != 200) {
                publicResourcesFileRetrievalQueueCount.getAndAdd(-1);
                publicResourcesFileMap.remove(serviceFileID);
                future.fail("[" + reply.correlationID() + "] Transfer of new public resource zip aborted by sender: " + reply.statusCode() + " File name: " + fileName + " Origin: " + origin);
                return;
            }
            final Message[] unpackConfirmationRequest = new Message[1];
            vertx.executeBlocking(receiveFuture -> {
                receiveFileViaTCP(reply,0,config().getJsonObject("publicHTTPServer").getLong("fileTransferTimeout"), config().getJsonObject("publicHTTPServer").getString("publicResourcesFolderPath"), receiveFuture, followUp -> {
                    if(followUp.succeeded() && followUp.statusCode() == 200) {
                        unpackConfirmationRequest[0] = followUp;
                    }
                });
            }, result -> {
                if(result.failed()) {
                    future.fail("[" + reply.correlationID() + "] Receiving new public resource zip failed. File name: " + fileName + " Origin: " + origin + " Cause: " + reply.cause());
                    publicResourcesFileMap.remove(serviceFileID);
                    publicResourcesFileRetrievalQueueCount.getAndAdd(-1);
                    return;
                }
                JsonObject fileInfo = (JsonObject) result.result();

                // unpack the file to folder
                vertx.executeBlocking(extractionFuture -> {
                    ZipUtils.UNZIP(
                            config().getJsonObject("publicHTTPServer").getString("publicResourcesFolderPath") + File.separator + relativeExtractionPath,
                            fileInfo.getString("filePath"),
                            config().getJsonObject("publicHTTPServer").getLong("maxTotalExtractedSizePerZipResourceInMB"),
                            config().getJsonObject("publicHTTPServer").getInteger("maxNumberOfFilesPerZipResource"),
                            extractionFuture);
                }, extractionResult -> {
                    if(extractionResult.failed()) {
                        publicResourcesFileMap.remove(serviceFileID);
                        if(unpackConfirmationRequest[0] != null) {
                            unpackConfirmationRequest[0].reply(500, "Extraction of the public resource zip failed: " + extractionResult.cause());
                        }
                        future.fail("[" + reply.correlationID() + "] Extraction of the public resource zip failed: " + extractionResult.cause());
                    }
                    else {
                        JsonObject extractionInfo = (JsonObject) extractionResult.result();
                        if(unpackConfirmationRequest[0] != null) {
                            unpackConfirmationRequest[0].reply(200, new JsonObject()
                                    .put("fileName", fileInfo.getString("fileName"))
                                    .put("fileSize", fileInfo.getLong("fileSize"))
                                    .put("numberOfFiles", extractionInfo.getInteger("numberOfFiles"))
                                    .put("totalExtractedSize", extractionInfo.getInteger("totalExtractedSize"))
                                    .put("relativeExtractionPath", relativeExtractionPath));
                        }

                        JsonObject availableInfo = new JsonObject()
                                .put("locationPath", relativeExtractionPath)
                                .put("locationService", serviceName)
                                .put("locationHost", hostLanAddress)
                                .put("sourceFile", fileInfo.getString("fileName"))
                                .put("sourceOrigin", origin);

                        publicResourcesFileMap.put(serviceFileID, availableInfo);
                        publish("publicResources", "1", "newAvailabilityInfo", availableInfo);


                        future.complete(new JsonObject()
                            .put("fileName", fileInfo.getString("fileName"))
                            .put("fileSize", fileInfo.getLong("fileSize"))
                            .put("fileOrigin", fileInfo.getString("fileOrigin"))
                            .put("numberOfFiles", extractionInfo.getInteger("numberOfFiles"))
                            .put("totalExtractedSize", extractionInfo.getInteger("totalExtractedSize"))
                            .put("targetDirectory", extractionInfo.getString("targetDirectory"))
                        );
                    }

                    publicResourcesFileRetrievalQueueCount.getAndAdd(-1);
                    File zipFile = new File(fileInfo.getString("filePath"));
                    zipFile.delete();
                    return;
                });
            });
        });
    }

    @Processor(
            domain = "in.sessions",
            version = "1",
            type = "setup",
            description = "Setup (create EventBus Address) session",
            requires = {
                    @Payload(key = "sessionSource", type = DataType.STRING, description = "Remote IP address, from which the session was initiated"),
                    @Payload(key = "sessionStartTimestamp", type = DataType.LONG, description = "Timestamp of session creation"),
                    @Payload(key = "sessionToken", type = DataType.STRING, description = "Token of the session"),
                    @Payload(key = "router", type = DataType.STRING, description = "EventBus Address of the corresponding router")
            }
    )
    public void processSessionSetup(Message message) {
        JsonObject messageBody = message.getBodyAsJsonObject();
        String sessionToken = messageBody.getString("sessionToken");
        User user = tokenCache.getIfPresent(sessionToken);

        // TODO: for high traffic setups (a lot of users logging in simultaneously) the following changes/improvements are required:
        // 1. do session setup when session token known/detected for the first time (access check above)
        // 2. create bridge by local invocation, without publish for reducing EB traffic
        // 3. only create bridge at WebBridge service which has the connection to the client

        if(user == null) {
            message.reply(404, "User session unknown!");
            return;
        }

        // create a bridge for sending messages to the client session
        publish("webBridge", "1", "missingBridge", new JsonObject()
            .put("domain","")
                .put("domain","out.userSessions")
                .put("version","1")
                .put("type", sessionToken)
                .put("isInbound", false)
                .put("isOutbound", true)
                // TODO: change to router
                .put("authorities", new JsonObject().put("sessions", new JsonArray().add(sessionToken)))
        );

        // add the address to the session cache
        user.setSessionAddress(sessionToken);

        JsonObject sessionInfo = new JsonObject()
                .put("sessionSource", messageBody.getString("sessionSource"))
                .put("sessionStartTimestamp", messageBody.getLong("sessionStartTimestamp"))
                .put("sessionToken", sessionToken)
                .put("sessionAddress", user.sessionAddress())
                .put("userID", user.id())
                .put("userRoles", user.roles().encode());

        publish("sessions", "1", "start", sessionInfo);

        // return the session infos
        message.reply(sessionInfo);
    }

    @Processor(
            domain = "sessions",
            version = "1",
            type = "end",
            description = "Removes session related settings (such as session address, token, ...)",
            requires = {
                    @Payload(key = "userID", type = DataType.STRING, description = "ID of the user"),
                    @Payload(key = "sessionSource", type = DataType.STRING, description = "Remote IP address, from which the session was initiated"),
                    @Payload(key = "sessionAddress", type = DataType.STRING, description = "Event Bus address for the session"),
                    @Payload(key = "sessionToken", type = DataType.STRING, description = "Token of the session")
            }
    )
    public void processSessionEnd(Message message) {
        JsonObject messageBody = message.getBodyAsJsonObject();

        // invalidate cache
        if(messageBody.getString("sessionToken") != null) {
            tokenCache.invalidate(messageBody.getString("sessionToken"));
        }
        // remove bridge
        if(messageBody.getString("sessionAddress") != null) {
            publish("webBridge", "1", "obsoleteBridge", new JsonObject()
                    .put("domain", "out.userSessions.1")
                    .put("version", "1")
                    .put("type", messageBody.getString("sessionToken"))
            );
        }
    }

    @Processor(
            domain = "publicResources",
            version = "1",
            type = "missingAvailabilityInfo",
            description = "Provides basic information on public resource files served (assuming all WebBridge instances serve the same)",
            provides = {
                    @Payload(key = "availabilityList", type = DataType.JSONArray, description = "Array of availability infos")
            }
    )
    public void processMissingPublicResourcesAvailabilityInfo(Message message) {
        if(!config().getBoolean("isPublicHTTPServerEnabled")) {
            message.reply(404, "This WebBridge does not serve public files!");
        }
        JsonArray list = new JsonArray();
        Enumeration<JsonObject> availabilityInfoEnumeration = publicResourcesFileMap.elements();
        while(availabilityInfoEnumeration.hasMoreElements()) {
            list.add(availabilityInfoEnumeration.nextElement());
        }
        message.reply(new JsonObject().put("availabilityList", list));
    }


    @Customer(
            domain = "users",
            version = "1",
            type = "missingPBEKeySpecForEMailUser",
            description = "Requests password hashing instructions so only a hashed password has to send to the authentication processor"
    )
    @Customer(
            domain = "users",
            version = "1",
            type = "unauthenticatedEMailUser",
            description = "Asks for a authentication of a user via email"
    )
    public void authenticateEMailUser(RoutingContext loginContext) {
        JsonObject authInfo = loginContext.getBodyAsJson();
        if (authInfo == null || authInfo.isEmpty()) {
            // bad request
            loginContext.fail(400);
            return;
        }
        String email = authInfo.getString("email");
        if (email == null || email.isEmpty() || !authInfo.containsKey("password")) {
            // also bad request
            loginContext.fail(400);
            return;
        }

        send("users", "1", "missingPBEKeySpecForEMailUser", new JsonObject().put("email", email), specReply -> {
            if(specReply.failed()) {
                loginContext.fail(500);
                return;
            }
            if(specReply.statusCode() != 200) {
                loginContext.fail(specReply.statusCode());
                return;
            }
            JsonObject PBEKeySpecs = specReply.getBodyAsJsonObject();

            vertx.executeBlocking(authFuture -> {
                String hashedPassword = hashPassword(authInfo.getString("password"),
                        PBEKeySpecs.getString("passwordKeyFactory"),
                        PBEKeySpecs.getString("passwordSalt"),
                        PBEKeySpecs.getInteger("passwordIterations"),
                        PBEKeySpecs.getInteger("passwordKeyLength"));
                authInfo.put("password", "");

                if(hashedPassword == null) {
                    loginContext.fail(500);
                    return;
                }

                send("users","1", "unauthenticatedEMailUser", specReply.correlationID(), new JsonObject()
                        .put("email", email)
                        .put("passwordHash", hashedPassword)
                        .put("sessionSource", loginContext.request().remoteAddress().host()),
                        authReply -> {
                    if(authReply.statusCode() != 200) {
                        loginContext.fail(authReply.statusCode());
                        return;
                    }

                    User user = new User(vertx, authReply.getBodyAsJsonObject());
                    loginContext.session().put("user", user);
                    loginContext.setUser(user);
                    tokenCache.put(user.sessionToken(), user);

                    loginContext.response()
                            .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                            .end(new JsonObject().put("xSessionToken", user.sessionToken()).encode());

                });
            }, resultHandler -> {});
        });
    }

    @Processor(
            domain = "webBridge",
            version = "1",
            type = "obsoleteBridge",
            description = "Removes a bridge between EventBus and outside world",
            requires = {
                    @Payload(key = "domain", type = DataType.STRING, description = "Address domain"),
                    @Payload(key = "version", type = DataType.STRING, description = "Address version"),
                    @Payload(key = "type", type = DataType.STRING, description = "Address type"),
            }
    )
    public void processObsoleteBridge(Message message) {
        JsonObject bridgingRequest= message.getBodyAsJsonObject();
        String domain = bridgingRequest.getString("domain");
        String version = bridgingRequest.getString("version");
        String type = bridgingRequest.getString("type");

        if(domain == null || domain.isEmpty()) {
            logger.warn("[" + message.correlationID() + "] " + message.origin() + ": dropping obsoleteBridge event due to erroneous payload: address");
            message.reply(400, "Address (domain) missing'");
            return;
        }
        if(version == null || version.isEmpty()) {
            logger.warn("[" + message.correlationID() + "] " + message.origin() + ": dropping obsoleteBridge event due to erroneous payload: address");
            message.reply(400, "version (domain) missing");
            return;
        }
        if(type == null || type.isEmpty()) {
            logger.warn("[" + message.correlationID() + "] " + message.origin() + ": dropping obsoleteBridge event due to erroneous payload: address");
            message.reply(400, "Address (domain) missing");
            return;
        }
        String address = domain + "." + version + "." + type;

        if(availableBridges.containsKey(address)) {
            if(inboundWhitelist.containsKey(address)) {
                inboundWhitelist.remove(address);
            }
            if(outboundWhitelist.containsKey(address)) {
                outboundWhitelist.remove(address);
            }
            availableBridges.remove(address);
            logger.info("[" + message.correlationID() + "] " + message.origin() + ": removed obsolete bridge at: " + address);
            message.reply(200);
            return;
        }
        else {
            message.reply(404);
        }
    }




    private static String hashPassword(final String password, final String keyFactory, final String saltString, final int iterations, final int keyLength) {
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance( keyFactory );
            byte[] salt = saltString.getBytes("UTF-8");
            PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, keyLength);
            SecretKey key = skf.generateSecret( spec );
            byte[] res = key.getEncoded( );
            return new Base64().encodeAsString(res);
        } catch( Exception e) {
            logger.warn("Error when hashing a user password", e);
            return null;
        }
    }

    private void loadConfig() {
        JsonObject config = config();

        if(!config.containsKey("port")) {
            config.put("port", 8080);
            logger.warn("Missing config entry: port. Using default: " + 8080);
        }

        if(!config.containsKey("isSSLEnabled")) {
            config.put("isSSLEnabled", false);
            logger.warn("Missing config entry: isSSLEnabled. Using default: " + false);
        }

        if(!config.containsKey("ssl")) {
            config.put("ssl", new JsonObject());
        }

        if(!config.containsKey("CORS")) {
            config.put("CORS", new JsonObject());
        }

        if(!config.getJsonObject("CORS").containsKey("allowedOriginPattern")) {
            config.getJsonObject("CORS").put("allowedOriginPattern", "");
            logger.warn("Missing config entry: CORS.allowedOriginPattern. Using default: ''");
        }

        if(!config.containsKey("isWebSocketBridgeEnabled")) {
            config.put("isWebSocketBridgeEnabled", true);
            logger.warn("Missing config entry: isWebSocketBridgeEnabled. Using default: " + true);
        }

        if(!config.containsKey("webSocketBridge")) {
            config.put("webSocketBridge", new JsonObject());
        }

        if(!config.getJsonObject("webSocketBridge").containsKey("heartbeatInterval")) {
            config.getJsonObject("webSocketBridge").put("heartbeatInterval", 2000);
            logger.warn("Missing config entry: webSocketBridge.heartbeatInterval. Using default: " + 2000);
        }

        if(!config.getJsonObject("webSocketBridge").containsKey("replyTimeout")) {
            config.getJsonObject("webSocketBridge").put("replyTimeout", 250000);
            logger.warn("Missing config entry: webSocketBridge.replyTimeout. Using default: " + 250000);
        }

        if(!config.containsKey("isPublicHTTPServerEnabled")) {
            config.put("isPublicHTTPServerEnabled", false);
            logger.warn("Missing config entry: isPublicHTTPServerEnabled. Using default: " + false);
        }

        if(!config.containsKey("publicHTTPServer")) {
            config.put("publicHTTPServer", new JsonObject());
        }

        if(!config.getJsonObject("publicHTTPServer").containsKey("isHTTP2Enabled")) {
            config.getJsonObject("publicHTTPServer").put("isHTTP2Enabled", false);
        }

        if(!config.getJsonObject("publicHTTPServer").containsKey("idleTimeoutInSeconds")) {
            config.getJsonObject("publicHTTPServer").put("idleTimeoutInSeconds", 600);
        }

        if(!config.getJsonObject("publicHTTPServer").containsKey("initialFileRequestWaitingTime")) {
            config.getJsonObject("publicHTTPServer").put("initialFileRequestWaitingTime", 30000);
        }

        if(!config.getJsonObject("publicHTTPServer").containsKey("fileTransferTimeout")) {
            config.getJsonObject("publicHTTPServer").put("fileTransferTimeout", 600000);
        }

        if(!config.getJsonObject("publicHTTPServer").containsKey("maxTotalExtractedSizePerZipResourceInMB")) {
            config.getJsonObject("publicHTTPServer").put("maxTotalExtractedSizePerZipResourceInMB", 100L);
        }

        if(!config.getJsonObject("publicHTTPServer").containsKey("maxNumberOfFilesPerZipResource")) {
            config.getJsonObject("publicHTTPServer").put("maxNumberOfFilesPerZipResource", 500);
        }

        if(!config.getJsonObject("publicHTTPServer").containsKey("publicResourcesFolderPath")) {
            config.getJsonObject("publicHTTPServer").put("publicResourcesFolderPath", "webroot");
        }

        if(!config.containsKey("isEMailBasedLoginEnabled")) {
            config.put("isEMailBasedLoginEnabled", false);
            logger.warn("Missing config entry: isEMailBasedLoginEnabled. Using default: " + false);
        }
        if(!config.containsKey("emailBasedLogin")) {
            config.put("emailBasedLogin", new JsonObject());
        }

    }

    @Override
    public void shutdown(Future<Object> shutdownFuture) {

        shutdownFuture.complete();

    }
}
