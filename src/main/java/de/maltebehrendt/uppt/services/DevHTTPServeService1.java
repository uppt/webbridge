package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.annotation.Customer;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.file.FileSystem;
import io.vertx.core.json.JsonObject;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.LinkedList;

import static java.nio.file.StandardWatchEventKinds.*;

/**
 * Created by malte on 03.04.17.
 */
public class DevHTTPServeService1 extends AbstractService  {
    private File destination = null;
    private Path sourcePath = null;
    private Path destinationPath = null;
    private WatchService watchService = null;
    private FileSystem fileSystem = null;
    private Long timerID = null;
    private HashMap<WatchKey, Path> watchKeys = null;

    @Override
    public void prepare(Future<Object> prepareFuture) {
        fileSystem = vertx.fileSystem();
        sourcePath = Paths.get(config().getString("sourceFolderPath"));
        File source = sourcePath.toFile();
        destinationPath = Paths.get(config().getString("destinationFolderPath"));
        destination = destinationPath.toFile();
        watchKeys = new HashMap<>();

        if(!source.exists() || !source.isDirectory()) {
            logger.error("Source directory" + source.getPath() + " does not exist or is not a directory!");
            prepareFuture.fail("Source directory" + source.getPath() + " does not exist or is not a directory!");
            return;
        }

        if(!destination.exists() || !destination.isDirectory()) {
            logger.error("Destination/resource directory" + destination.getPath() + " does not exist or is not a directory!");
            prepareFuture.fail("Destination/resource directory" + destination.getPath() + " does not exist or is not a directory!");
            return;
        }

        prepareFuture.complete();
    }

    @Override
    @Customer(
            domain = "webBridge",
            version = "1",
            type = "missingBridge",
            description = "Only here to express dependency on webBridge service"
    )
    public void startConsuming() {
        // initial copying of files... (async, just assuming it will go through smoothly and that there are no significant side effects later on...)
        LinkedList<Future> copyFutures = new LinkedList<>();
        for(File file : sourcePath.toFile().listFiles()) {
            Future copyFuture = Future.future();
            copyFutures.add(copyFuture);
            copyFile(file.toPath(), copyFuture);
        }

        CompositeFuture.all(copyFutures).setHandler(result -> {
            if(result.succeeded()) {
                // start watching service and add all sub-folders
                try {
                    watchService = sourcePath.getFileSystem().newWatchService();

                    Files.walkFileTree(sourcePath, new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                            WatchKey watchKey = dir.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
                            watchKeys.put(watchKey, dir);
                            return FileVisitResult.CONTINUE;
                        }
                    });
                }
                catch(Exception e) {
                    logger.fatal("Failed to start watch service for detecting file updates!", e);
                    System.exit(-1);
                }

                // notify of availability
                publish("devHTTPServeService", "1", "watcherReady", new JsonObject());
                logger.info("DevHTTPServiceService up and running...");

                // check for changes periodically...
                timerID = vertx.setPeriodic(250, handler -> {
                    try {
                        WatchKey watchKey = watchService.poll();

                        if(watchKey == null) {
                            return;
                        }

                        Path folderPath = watchKeys.get(watchKey);
                        if(folderPath == null) {
                            logger.warn("Experienced unknown/unregistered path event.");
                            return;
                        }


                        for(WatchEvent<?> event : watchKey.pollEvents()) {
                            WatchEvent.Kind kind = event.kind();

                            if(kind == OVERFLOW) {
                                logger.warn("Overflow when watching file changes in " + config().getString("sourceFolderPath"));
                                continue;
                            }
                            else if(kind == ENTRY_DELETE) {
                                Path filePath = folderPath.resolve((((WatchEvent<Path>) event).context()));
                                delFile(filePath, null);
                            }
                            else if(kind == ENTRY_CREATE) {
                                Path filePath = folderPath.resolve((((WatchEvent<Path>) event).context()));
                                File file = filePath.toFile();

                                if(file.isDirectory()) {
                                    Future copyFuture = Future.future();
                                    copyFuture.setHandler(copyHandler -> {
                                        if(copyFuture.failed()) {
                                            logger.error("Failed to copy new folder: " + filePath, copyFuture.cause());
                                        }
                                        else {
                                            try {
                                                WatchKey key = filePath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
                                                watchKeys.put(key, filePath);
                                            } catch (IOException e) {
                                                logger.error("Failed to add new folder to watch service: " + filePath, e);
                                            }
                                        }
                                    });
                                    copyFile(filePath, copyFuture);
                                }
                                else if(file.isFile()) {
                                    // ignore for now - adding content to a file leads to a modification event
                                }
                            }
                            else if(kind == ENTRY_MODIFY){
                                // copy new file (version)
                                Path filePath = folderPath.resolve((((WatchEvent<Path>) event).context()));
                                copyFile(filePath, null);
                            }
                        }

                        if(!watchKey.reset()) {
                            logger.info("Watch key is not valid anymore (watched directory inaccessible): " + folderPath);
                            watchKeys.remove(watchKey);
                        }
                    }
                    catch(Exception e) {
                        logger.error("Processing file events in folder " + config().getString("sourceFolderPath") + " caused an exception!", e);
                    }
                });
            }
            else {
                logger.fatal("Failed to copy source files to target directory!", result.cause());
                System.exit(-1);
            }
        });
    }

    private void delFile(Path sourceFilePath, Future delFuture) {
        Path targetPath = destinationPath.resolve(sourcePath.relativize(sourceFilePath));
        File target = targetPath.toFile();

        if(!target.exists()) {
            return;
        }

        if(target.isDirectory()) {
            fileSystem.deleteRecursive(targetPath.toAbsolutePath().toString(), true, result -> {
                if(result.failed()) {
                    logger.error("Failed to delete folder: " + targetPath.toString(), result.cause());
                    if(delFuture != null) { delFuture.fail(result.cause()); }
                }
                else if(delFuture != null) {
                    delFuture.complete();
                }
            });
        }
        else if (target.isFile()) {
            fileSystem.delete(targetPath.toAbsolutePath().toString(), result -> {
               if(result.failed()) {
                   logger.error("Failed to delete file: " + targetPath.toString(), result.cause());
                   if(delFuture != null) { delFuture.fail(result.cause()); }
               }
               else if(delFuture != null) {
                   delFuture.complete();
               }
            });
        }
    }

    private void copyFile(Path sourceFilePath, Future copyFuture) {
        Path targetPath = destinationPath.resolve(sourcePath.relativize(sourceFilePath));
        File target = targetPath.toFile();

        if(target.exists()) {
            if(target.isDirectory()) {
                fileSystem.deleteRecursiveBlocking(targetPath.toAbsolutePath().toString(), true);
            }
            else if(target.isFile()) {
                fileSystem.deleteBlocking(targetPath.toAbsolutePath().toString());
            }
        }

        fileSystem.copyRecursive(sourceFilePath.toAbsolutePath().toString(), targetPath.toAbsolutePath().toString(), true, result -> {
            if(result.failed()) {
                logger.error("Failed to copy: " + sourceFilePath.toString(), result.cause());
                if(copyFuture != null) { copyFuture.fail(result.cause()); }
            }
            else if(copyFuture != null) {
                copyFuture.complete();
            }
        });
    }

    @Override
    public void shutdown(Future<Object> shutdownFuture) {
        if(timerID != null) {
            vertx.cancelTimer(timerID);
        }

        try {
            watchService.close();
        } catch (IOException e) {
            logger.warn("Failed to close watch service", e);
        }
        shutdownFuture.complete();
    }
}
