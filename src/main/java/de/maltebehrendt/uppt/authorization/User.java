package de.maltebehrendt.uppt.authorization;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AbstractUser;
import io.vertx.ext.auth.AuthProvider;


/**
 * Created by malte on 01.02.17.
 */
public class User extends AbstractUser {
    private Vertx vertx = null;
    private JsonObject principial = null;

    public User(Vertx vertx, JsonObject principial) {
        if(principial == null) {
            principial = new JsonObject();
        }
        if(!principial.containsKey("roles")) {
            principial.put("roles", new JsonArray());
        }
        if(!principial.containsKey("id")) {
            principial.put("id", "");
        }
        if(!principial.containsKey("sessionToken")) {
            principial.put("sessionToken", "");
        }
        if(!principial.containsKey("sessionSource")) {
            principial.put("sessionSource", "");
        }
        if(!principial.containsKey("sessionAddress")) {
            principial.put("sessionAddress", "");
        }
        this.principial = principial;
        this.vertx = vertx;
    }

    @Override
    protected void doIsPermitted(String permission, Handler<AsyncResult<Boolean>> resultHandler) {
        /*vertx.executeBlocking(future -> {
            if(permission.startsWith("role:")) {
                String requiredRole = permission.substring(5);
                if(principial.getJsonArray("roles").contains(requiredRole)) {
                    future.complete(true);
                }
                else {
                    future.complete(false);
                }
            }
            else {
                future.complete(false);
            }
        }, resultHandler);
*/
        //TODO: if creation timestamp too old, then renew permissions
    }

    @Override
    public JsonObject principal() {
        return principial;
    }

    public String id()  {
        return principal().getString("id");
    }
    public JsonArray roles()  {
        return principal().getJsonArray("roles");
    }
    public String sessionToken() {
        return principal().getString("sessionToken");
    }
    public String sessionSource() {
        return principal().getString("sessionSource");
    }
    public String sessionAddress() {
        return principal().getString("sessionAddress");
    }
    public void setSessionAddress(String sessionAddress) {
        principal().put("sessionAddress", sessionAddress);
    }

    @Override
    public void setAuthProvider(AuthProvider authProvider) {
        // ignored on purpose for now
    }
}
