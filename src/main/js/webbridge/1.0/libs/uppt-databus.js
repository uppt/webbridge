(function(window) {
    'use strict';

    function initDataBus() {
        // private vars and functions
        var subscriptions = {};
        var localData = {"session": {"1": {}}, "config": {"1": {}}};

        var eventBus = null;
        var sessionToken = null;

        // get a value from the local/cached data
        var getLocal = function(domain, version, jsonPath) {
            if(!localData[domain][version]) {
                if(!localData[domain]) {
                    localData[domain] = {};
                }
                if(!localData[domain][version]) {
                    localData[domain][version] = {};
                }
            }
            return JSONPath(localData[domain][version], jsonPath);
        };

        // set a value in the local/cached data
        // very difficult to read and not well performing. To be improved...
        var setLocal = function(domain, version, absolutePath, value) {
            if(!absolutePath || !version || !domain) {
                return false;
            }
            if(absolutePath.startsWith("$.")) {
                absolutePath = absolutePath.substring(2);
            }
            // make sure domain.version is always an object. Otherwise version 1 and 1.1 will cause exceptions
            if(!localData[domain]) {
                localData[domain] = {};
            }
            if(!localData[domain][version]) {
                localData[domain][version] = {};
            }
            var tokens = absolutePath.split(".");
            var arrayPattern = /(?:\[(.+?)\])/g
            var numberPattern = /^\d+$/;
            var currentPosition = localData[domain][version];
            for(var i=0;i<tokens.length;i++) {
                var token = tokens[i];
                var matches = token.match(arrayPattern);
                if(matches !== null) {
                    matches.unshift(token.substring(0, token.indexOf("[")));
                    for(var a=1;a<matches.length;a++) {
                        matches[a] = matches[a].substring(1, matches[a].length-1).replace(/[\"']/g, "");
                        if(matches[a].match(numberPattern)) {
                            matches[a] = parseInt(matches[a]);
                        }
                    }
                    var previousPosition = null;
                    for(var a=0;a<matches.length;a++) {
                        if(!currentPosition[matches[a]]) {
                            if(typeof(matches[a+1]) === "number") {
                                currentPosition[matches[a]] = [];
                            }
                            else {
                                currentPosition[matches[a]] = {};
                            }
                        }
                        i=i+1;
                        tokens.splice(i, 0, matches[a]);
                        previousPosition = currentPosition;
                        currentPosition = currentPosition[matches[a]];
                    }
                    if(i === tokens.length -1) {
                        previousPosition[matches[matches.length-1]] = value;
                        return true;
                    }
                }
                else {
                    if(!currentPosition[token]) {
                        if(i === tokens.length -1) {
                            currentPosition[token] = value;
                            return true;
                        }
                        else {
                            currentPosition[token] = {};
                            currentPosition = currentPosition[token];
                        }
                    }
                    else if(i === tokens.length -1) {
                        currentPosition[token] = value;
                        return true;
                    }
                    else {
                        currentPosition = currentPosition[token];
                    }
                }
            }
            return false;
        };

        var forwardOperationToSolution = function(remoteOperation, domain, version, path, value, origin) {
            return new Promise(function(resolve, reject) {
                if(localData.session['1'].status !== "connected" || !eventBus) {
                    reject("Not connected!");
                    return;
                }

                eventBus.send(window.dataBusSettings.router, {"xSessionToken": sessionToken, "correlationID": generateID, "requests": [{
                    "domain": domain,
                    "version": version,
                    "path": path,
                    "value": value !== null? value : "",
                    "operation": remoteOperation,
                    "origin": origin
                    }]}, function(error, reply) {
                        if(error != null || !reply.headers) {
                            DataBus.logger.error("DataBus operation " + operation + " on " + path + " caused an error: " + error, reply.headers.correlationID? reply.headers.correlationID:"unknownCID");
                            reject(error);
                            return;
                        }
                        if(reply.headers.statusCode != 200) {
                            DataBus.logger.error("[" + reply.headers.correlationID + "] DataBus operation " + operation + " on " + path + " caused an error: " + body.message, reply.headers.correlationID? reply.headers.correlationID:"unknownCID");
                            reject(body.message);
                            return;
                        }
                        reply.body.results[0].correlationID = reply.headers.correlationID;
                        resolve(reply.body.results[0]);
                });
            });
        };

        // handle a databus get/set/delete operation
        var publishOrSend = function(operation, domain, version, path, jsonPath, value, origin, correlationID) {
            return new Promise(function(resolve, reject) {
                if(origin === null) {
                    origin = "unknown";
                }
                if(!operation) {
                    reject("Must provide the operation get-/set-/del- Local/Global!");
                    return;
                }
                if(!domain || !version) {
                    reject("Must provide domain and version!");
                    return;
                }
                if(!correlationID) {
                    correlationID = generateID();
                }
                var observerNotifier = function(result) {
                    for(var index in result.paths) {
                        var path = result.paths[index];
                        var observerMessage = {"operation": result.operation, "paths": [path], "jsonPath": result.jsonPath, "values": [result.values[index]], "domain": domain, "version": version, "correlationID": result.correlationID, "origin": result.origin};
                        for(var id in subscriptions) {
                            if(subscriptions[id].domain === domain && subscriptions[id].version === version) {
                                if(subscriptions[id].absolute && path === subscriptions[id].absolute && typeof(subscriptions[id].onNext) === 'function') {
                                    try {
                                        subscriptions[id].onNext(observerMessage, subscriptions[id]);
                                    }
                                    catch(e) {
                                        DataBus.logger.error("[" + result.correlationID + "] Prefix callback " + id + " for " + path + " caused an exception: " + e);
                                    }
                                }
                                else if(subscriptions[id].prefix && path.startsWith(subscriptions[id].prefix) && typeof(subscriptions[id].onNext) === 'function') {
                                    try {
                                        subscriptions[id].onNext(observerMessage, subscriptions[id]);
                                    }
                                    catch(e) {
                                        DataBus.logger.error("[" + result.correlationID + "] Prefix callback " + id + " for " + path + " caused an exception: " + e);
                                    }
                                }
                                else if(subscriptions[id].pattern && path.match(subscriptions[id].pattern) !== null && typeof(subscriptions[id].onNext) === 'function') {
                                    try {
                                        subscriptions[id].onNext(observerMessage, subscriptions[id]);
                                    }
                                    catch(e) {
                                        DataBus.logger.error("[" + result.correlationID + "] Pattern callback " + id + " for " + path + " caused an exception: " + e);
                                    }
                                }
                            }
                        }
                    }
                };

                if(localData.session['1'].status !== "connected" || !eventBus) {
                    // TODO: make use of retry/cache/...
                    switch(operation) {
                        case "get":
                            DataBus.logger.warn("[" + correlationID + "] Not connected, operation 'get' only performed on local cache!");
                            operation = "getLocal";
                            break;
                        case "set":
                            DataBus.logger.warn("[" + correlationID + "] Not connected, operation 'set' only performed on local cache!");
                            operation = "setLocal";
                            break;
                        case "del":
                            DataBus.logger.warn("[" + correlationID + "] Not connected, operation 'del' only performed on local cache!");
                            operation = "delLocal";
                            break;
                    }
                }

                switch(operation) {
                    case "get":
                        // try local data first, only send to server if no local data available
                        var resultObject = getLocal(domain, version, jsonPath);
                        if(resultObject && resultObject.values && resultObject.paths && resultObject.values.length > 0) {
                            var result = {"operation": operation, "domain": domain, "version": version, "paths": resultObject.paths, "jsonPath": jsonPath, "origin": origin, "correlationID": correlationID, "values": resultObject.values};
                            resolve(result);
                            return;
                        }
                        // else: go on to get remote
                    case "getRemote":
                        forwardOperationToSolution("GET", domain, version, jsonPath, value, origin)
                            .then(function(result) {
                                result.operation = operation;
                                result.jsonPath = jsonPath;

                                resolve(result);

                                // also store result locally and notify observers...
                                for(var i=0;i<result.paths.length;i++) {
                                    if(setLocal(domain, version, result.paths[i], result.values[i])) {
                                        var cacheResult = {"operation": operation, "domain": domain, "version": version, "paths": [result.paths[i]], "jsonPath": "", "origin": origin, "correlationID": result.correlationID, "values": [result.values[i]]};
                                        observerNotifier(cacheResult);
                                    }
                                    else {
                                        DataBus.logger.error("Storing remotely retrieved value locally failed! " + domain +"."+ version+":"+  result.paths[i] + ": " + result.values[i], result.correlationID );
                                    }
                                }
                            })
                            .catch(function(error) {
                                reject(error);
                            });
                        break;
                    case "set":
                    case "setRemote":
                        forwardOperationToSolution("SET", domain, version, path, value, origin)
                            .then(function(result) {
                                result.operation = operation;
                                resolve(result);

                                // also store result locally and notify observers...
                                for(var i=0;i<result.paths.length;i++) {
                                    if(setLocal(domain, version, result.paths[i], result.values[i])) {
                                        var cacheResult = {"operation": operation, "domain": domain, "version": version, "paths": [result.paths[i]], "jsonPath": "", "origin": origin, "correlationID": result.correlationID, "values": [result.values[i]]};
                                        observerNotifier(cacheResult);
                                    }
                                    else {
                                        DataBus.logger.error("Storing remotely set value locally failed! " + domain +"."+ version+":"+  result.paths[i] + ": " + result.values[i], result.correlationID);
                                    }
                                }
                            })
                            .catch(function(error) {
                                reject(error);
                            });
                        break;
                    case "del":
                    case "delRemote":
                        forwardOperationToSolution("DEL", domain, version, path, "", origin)
                        .then(function(result) {
                            result.operation = operation;
                            result.values[0] = null;
                            resolve(result);

                            // also store result locally and notify observers...
                            for(var i=0;i<result.paths.length;i++) {
                                if(setLocal(domain, version, result.paths[i], null)) {
                                    var cacheResult = {"operation": operation, "domain": domain, "version": version, "paths": [result.paths[i]], "jsonPath": "", "origin": origin, "correlationID": result.correlationID, "values": [null]};
                                    observerNotifier(cacheResult);
                                }
                                else {
                                    DataBus.logger.error("Deleting (setting to null) remotely deleted value locally failed! " + domain +"."+ version+":"+  result.paths[i] + ": " + result.values[i], result.correlationID);
                                }
                            }
                        })
                        .catch(function(error) {
                            reject(error);
                        });
                        break;
                    case "getLocal":
                        var resultObject = getLocal(domain, version, jsonPath);
                        if(resultObject !== null) {
                            var result = {"operation": operation, "domain": domain, "version": version, "paths": resultObject.paths, "jsonPath": jsonPath, "origin": origin, "correlationID": correlationID, "values": resultObject.values};
                            resolve(result);
                        }
                        else {
                            reject("[" + correlationID + "] getLocal failed! Check that correct jsonPath was provided!");
                        }
                        break;
                    case "setPush":
                    case "setLocal":
                        if(setLocal(domain, version, path, value)) {
                            var result = {"operation": operation, "domain": domain, "version": version, "paths": [path], "jsonPath": "", "origin": origin, "correlationID": correlationID, "values": [value]};
                            resolve(result);
                            observerNotifier(result);
                        }
                        else {
                            reject("[" + correlationID + "] Setting local data failed!");
                        }
                        break;
                    case "delLocal":
                        if(setLocal(domain, version, path, null)) {
                            var result = {"operation": operation, "domain": domain, "version": version, "paths": [path], "jsonPath": "", "origin": origin, "correlationID": generateID(), "values": [null]};
                            resolve(result);
                            observerNotifier(result);
                        }
                        else {
                            reject("[" + correlationID + "] Deleting (setting to null) local data failed!");
                        }
                        break;
                    default:
                        reject("Operation " + operation + " is not known/unsupported!");
                        break;
                }
            });
        };

        var loggerImpl = {
             log: function(message, correlationID) {
                    if(!correlationID) { correlationID = "unknownCID"; }
                    if(console.log) {
                        console.log("[" , correlationID , "] info : " , message);
                    }
                },
             info: function(message, correlationID) {
                     if(!correlationID) { correlationID = "unknownCID"; }
                     if(console.info) {
                         console.info("[" , correlationID , "] info : " , message);
                     }
                     else if(console.log) {
                         console.log("[" , correlationID , "] info : " , message);
                     }
                 },
             warn: function(message, correlationID) {
                     if(!correlationID) { correlationID = "unknownCID"; }
                     if(console.warn) {
                         console.warn("[" + correlationID + "] WARN : " + message);
                     }
                     else if(console.log) {
                         console.log("[" + correlationID + "] WARN : " + message);
                     }
                 },
             error: function(message, correlationID) {
                 if(!correlationID) { correlationID = "unknownCID"; }
                 if(console.error) {
                     console.error("[" + correlationID + "] ERROR : " + message);
                 }
                 else if(console.log) {
                     console.log("[" + correlationID + "] ERROR : " + message);
                 }
             },
             fatal: function(message, correlationID) {
                 if(!correlationID) { correlationID = "unknownCID"; }
                 if(console.error) {
                     console.error("[" + correlationID + "] FATAL : " + message);
                 }
                 else if(console.log) {
                     console.log("[" + correlationID + "] FATAL : " + message);
                 }
             }
        };

        var logoutImpl = function() {
            DataBus.logger.info("Logging out and closing UPPT connection.");
            eventBus.close();
            DataBus.setLocal("session", "1", "status", "disconnected", "DataBus.logout");
        };

        var deleteLocalStorage = function() {
            localStorage.setItem('email', "");
            localStorage.setItem('password', "");
        };

        var generateID = function() {
              return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (a, b) {
                return b = Math.random() * 16, (a == 'y' ? b & 3 | 8 : b | 0).toString(16);
           });
        };

        var DataBus = {
            // public attributes and methods
            logger: loggerImpl,
            subscribe: function(domain, version, pathSelector, isGetValueNow, onNext, onError) {
                if(typeof(isGetValueNow) === "function") {
                    onError = onNext;
                    onNext = isGetValueNow;
                    isGetValueNow = false;
                }
                if(!domain || !version || !pathSelector || pathSelector.length < 1) {
                    onError("Must provide domain, version, path selector!");
                }
                try {
                    var id = generateID();
                    var subscription = {
                        "id": id,
                        "domain": domain,
                        "version": version,
                        "onNext": onNext,
                        "onError": onError,
                        "unsubscribe": function() {
                            if(!id) {
                                return false;
                            }
                            return delete subscriptions[id];
                        }
                    };

                    if(typeof(pathSelector) === "string") {
                        if(pathSelector.endsWith(".*")) {
                            subscription.prefix = pathSelector.substring(0, pathSelector.length - 1);
                        }
                        else {
                            // TODO remove invalid characters (for absolute path)
                            subscription.absolute = pathSelector;
                        }

                        subscriptions[id] = subscription;
                        // retrieve current value
                        if(isGetValueNow) {
                            DataBus.get(domain, version, pathSelector, "subscription")
                                .then(function(result) {onNext(result, subscription);})
                                .catch(onError);
                            return subscription;
                        }
                    }
                    else if(pathSelector instanceof RegExp) {
                        if(isGetValueNow) {
                            // DOES NOT retrieve current value (that's a todo - walk current data and apply pattern matching efficiently)
                            onError("getValueNow is not supported for RegEx subscriptions!");
                            return;
                        }
                        subscription.pattern = pathSelector;
                        subscriptions[id] = subscription;
                        return subscription;
                    }
                }
                catch(error) {
                    onError(error);
                }
            },
            unsubscribe: function(subscriptionID) {
                if(!subscriptionID) {
                    return false;
                }
                return delete subscriptions[subscriptionID];
            },
            get: function(domain, version, jsonPath, origin) {
                return publishOrSend("get", domain, version, "", jsonPath, "", origin);
            },
            set: function(domain, version, absolutePath, value, origin) {
                return publishOrSend("set", domain, version, absolutePath, "", value, origin);
            },
            del: function(domain, version, absolutePath, origin) {
                return publishOrSend("del", domain, version, absolutePath, "", "", origin);
            },
            getRemote: function(domain, version, jsonPath, origin) {
                return publishOrSend("getRemote", domain, version, "", jsonPath, "", origin);
            },
            setRemote: function(domain, version, absolutePath, value, origin) {
                return publishOrSend("setRemote", domain, version, absolutePath, "", value, origin);
            },
            delRemote: function(domain, version, absolutePath, origin) {
                return publishOrSend("delRemote", domain, version, absolutePath, "", "", origin);
            },
            getLocal: function(domain, version, jsonPath, origin) {
                return publishOrSend("getLocal", domain, version, "", jsonPath, "", origin);
            },
            setLocal: function(domain, version, absolutePath, value, origin) {
                return publishOrSend("setLocal", domain, version, absolutePath, "", value, origin);
            },
            delLocal: function(domain, version, absolutePath, origin) {
                return publishOrSend("delLocal", domain, version, absolutePath, "", "", origin);
            },
            // TODO: anonymous login...
            loginViaEMail: function(email, password, isLocalStorageAllowed) {
                return new Promise(function(resolve, reject) {
                    var that = this;

                    var xmlHttpRequest = new XMLHttpRequest();

                    xmlHttpRequest.onreadystatechange = function() {
                        if(xmlHttpRequest.readyState === 4) {
                            if(xmlHttpRequest.status === 200) {
                                sessionToken = JSON.parse(xmlHttpRequest.responseText).xSessionToken;

                                if(isLocalStorageAllowed) {
                                    localStorage.setItem('email', email);
                                    localStorage.setItem('password', password !== null ? window.btoa(password) : null);
                                }
                                else {
                                    deleteLocalStorage();
                                }

                                eventBus = new EventBus(window.dataBusSettings.location + "/eventbus/websocket");
                                eventBus.onopen = function() {
                                    eventBus.send("in.sessions.1.setup", {"xSessionToken": sessionToken, "router": window.dataBusSettings.router}, function(error, reply) {
                                        if(error != null || !reply.headers || reply.headers.statusCode != 200) {
                                            DataBus.logger.error("Session setup caused an error: " + error, reply.headers.correlationID? reply.headers.correlationID:"unknownCID");
                                            reject(error);
                                            return;
                                        }
                                        var correlationID = reply.headers.correlationID;
                                        eventBus.registerHandler("out.userSessions.1." + reply.body.sessionAddress, {"xSessionToken": sessionToken, "correlationID": correlationID}, function(error, message) {
                                            if(error) {
                                                DataBus.logger.error("Failed to receive a message from the session address: " + error, message.headers? message.headers.correlationID : correlationID);
                                            }
                                            else if(message.headers.statusCode !== "200") {
                                                DataBus.logger.error("Received a push error (" + message.headers.statusCode + ") message: " + message.body.message, message.headers? message.headers.correlationID : correlationID);
                                            }
                                            else if(!message.body.results) {
                                                DataBus.logger.error("Received an invalid push message: " + message.body.message, message.headers? message.headers.correlationID : correlationID);
                                            }
                                            else {
                                                var results = message.body.results;
                                                for(var a=0;a<results.length;a++) {
                                                    for(var i=0;i<results[a].paths.length;i++) {
                                                        publishOrSend("setPush", results[a].domain, results[a].version, results[a].paths[i], "", results[a].values[i], results[a].origin, message.headers.correlationID);
                                                    }
                                                }
                                            }
                                        });

                                        var userInfo = {
                                            "id": reply.body.userID,
                                            "roles": JSON.parse(reply.body.userRoles)
                                        };
                                        DataBus.setLocal("session", "1", "address", reply.body.sessionAddress, "DataBus.loginViaEMail");
                                        DataBus.setLocal("session", "1", "user", userInfo, "DataBus.loginViaEMail");
                                        DataBus.setLocal("session", "1", "status", "connected", "DataBus.loginViaEMail")
                                            .then(function() {
                                                resolve({"user": userInfo, "address": reply.body.sessionAddress, "status": "connected"});
                                            })
                                            .catch(function(error) {
                                                DataBus.logger.error("DataBus apparently not available: " + error.message);
                                                reject(error);
                                            });
                                    });
                                };
                            }
                            else {
                                DataBus.logger.error("Failed to login via email: " + error, reply.headers.correlationID);
                                DataBus.setLocal("session", "1", "status", "disconnected", "DataBus.loginViaEMail");
                                reject(xmlHttpRequest.status);
                            }
                        }
                    };

                    DataBus.setLocal("session", "1", "status", "loggingIn", "DataBus.loginViaEMail");
                    xmlHttpRequest.setRequestHeader("Content-Type", "application/json");
                    xmlHttpRequest.open("POST", window.dataBusSettings.location + "/email/login", true);
                    xmlHttpRequest.send(JSON.stringify({"email": email, "password": password}));

                });
            },
            relogin: function() {
                 return new Promise(function(resolve, reject) {
                     // check if saved login is available
                     if(localStorage.getItem('email') && localStorage.getItem('password')) {
                         // attempt re-login
                         DataBus.loginViaEMail(localStorage.getItem('email'), window.atob(localStorage.getItem('password')), true)
                             .then(function(sessionInfo) {
                                 resolve(sessionInfo);
                             })
                             .catch(function(statusCode) {
                                 DataBus.setLocal("session", "1", "status", "disconnected", "DataBus.relogin");
                                 reject("(Re-) Login failed: " + statusCode);
                             });
                     }
                     else {
                         //publish need for login (disconnected status)
                         DataBus.setLocal("session", "1", "status", "disconnected", "DataBus.relogin");
                         reject("Login credentials not available/saved!");
                     }
                 });
             }
        };

        // listen to session status changes and process logout requests (status = loggingOut)
        DataBus.subscribe("session", "1", "status", function(update) {
            if(update.values[0] === "loggingOut") {
                logoutImpl();
            }
        });

        // check that all required settings are available
        // not provided via login function for easier (later) compatibility with OAuth procedures
        if(typeof(window.dataBusSettings) !== "object") {
           DataBus.logger.fatal("DataBus settings not set at global variable 'dataBusSettings'!");
           return null;
        }
        if(typeof(window.dataBusSettings.location) != "string") {
           DataBus.logger.fatal("UPPT location (URL) not set via global variable 'dataBusSettings.location'!");
           return null;
        }
        if(typeof(window.dataBusSettings.router) != "string") {
           DataBus.logger.fatal("UPPT router (EventBus Address) not set via global variable 'dataBusSettings.router'!");
           return null;
        }

        // save all settings/configuration in local data
        for(var property in window.dataBusSettings) {
            localData.config["1"][property] = window.dataBusSettings[property];
        }

        // attempt re-login
        DataBus.relogin();

        return DataBus;
    };

    // define globally, if not yet available
    if(typeof(DataBus) === 'undefined') {
       window.DataBus = initDataBus();
    }
    else {
        DataBus.logger.warn("DataBus library already defined.");
    }
})(window);

/* Adopted from JSONPath 0.8.5 - XPath for JSON
 *
 * Copyright (c) 2007 Stefan Goessner (goessner.net)
 * modified by Malte Behrendt (dev@maltebehrendt.de)
 * Licensed under the MIT (MIT-LICENSE.txt) licence.
 *
 * Proposal of Chris Zyp goes into version 0.9.x
 * Issue 7 resolved
 */

function JSONPath(obj, expr, arg) {
   var P = {
      result: { "paths": [], "values": []},
      normalize: function(expr) {
         var subx = [];
         return expr.replace(/[\['](\??\(.*?\))[\]']|\['(.*?)'\]/g, function($0,$1,$2){return "[#"+(subx.push($1||$2)-1)+"]";})  /* http://code.google.com/p/jsonpath/issues/detail?id=4 */
                    .replace(/'?\.'?|\['?/g, ";")
                    .replace(/;;;|;;/g, ";..;")
                    .replace(/;$|'?\]|'$/g, "")
                    .replace(/#([0-9]+)/g, function($0,$1){return subx[$1];});
      },
      asPath: function(path) {
         var x = path.split(";"), p = "";
         for (var i=1,n=x.length; i<n; i++)
            p += /^[0-9*]+$/.test(x[i]) ? ("["+x[i]+"]") : ("."+x[i]+"");
         return p.substring(1);
      },
      store: function(p, v) {
        if(p) {
            var index = P.result.paths.length;
            P.result.paths[index] = P.asPath(p);
            P.result.values[index] = v;
        }
        return !!p;
      },
      trace: function(expr, val, path) {
         if (expr !== "") {
            var x = expr.split(";"), loc = x.shift();
            x = x.join(";");
            if (val && val.hasOwnProperty(loc))
               P.trace(x, val[loc], path + ";" + loc);
            else if (loc === "*")
               P.walk(loc, x, val, path, function(m,l,x,v,p) { P.trace(m+";"+x,v,p); });
            else if (loc === "..") {
               P.trace(x, val, path);
               P.walk(loc, x, val, path, function(m,l,x,v,p) { typeof v[m] === "object" && P.trace("..;"+x,v[m],p+";"+m); });
            }
            else if (/^\(.*?\)$/.test(loc)) // [(expr)]
               P.trace(P.eval(loc, val, path.substr(path.lastIndexOf(";")+1))+";"+x, val, path);
            else if (/^\?\(.*?\)$/.test(loc)) // [?(expr)]
               P.walk(loc, x, val, path, function(m,l,x,v,p) { if (P.eval(l.replace(/^\?\((.*?)\)$/,"$1"), v instanceof Array ? v[m] : v, m)) P.trace(m+";"+x,v,p); }); // issue 5 resolved
            else if (/^(-?[0-9]*):(-?[0-9]*):?([0-9]*)$/.test(loc)) // [start:end:step] python slice syntax
               P.slice(loc, x, val, path);
            else if (/,/.test(loc)) { // [name1,name2,...]
               for (var s=loc.split(/'?,'?/),i=0,n=s.length; i<n; i++)
                  P.trace(s[i]+";"+x, val, path);
            }
         }
         else
            P.store(path, val);
      },
      walk: function(loc, expr, val, path, f) {
         if (val instanceof Array) {
            for (var i=0,n=val.length; i<n; i++)
               if (i in val)
                  f(i,loc,expr,val,path);
         }
         else if (typeof val === "object") {
            for (var m in val)
               if (val.hasOwnProperty(m))
                  f(m,loc,expr,val,path);
         }
      },
      slice: function(loc, expr, val, path) {
         if (val instanceof Array) {
            var len=val.length, start=0, end=len, step=1;
            loc.replace(/^(-?[0-9]*):(-?[0-9]*):?(-?[0-9]*)$/g, function($0,$1,$2,$3){start=parseInt($1||start);end=parseInt($2||end);step=parseInt($3||step);});
            start = (start < 0) ? Math.max(0,start+len) : Math.min(len,start);
            end   = (end < 0)   ? Math.max(0,end+len)   : Math.min(len,end);
            for (var i=start; i<end; i+=step)
               P.trace(i+";"+expr, val, path);
         }
      },
      eval: function(x, _v, _vname) {
         try { return $ && _v && eval(x.replace(/(^|[^\\])@/g, "$1_v").replace(/\\@/g, "@")); }  // issue 7 : resolved ..
         catch(e) { throw new SyntaxError("jsonPath: " + e.message + ": " + x.replace(/(^|[^\\])@/g, "$1_v").replace(/\\@/g, "@")); }  // issue 7 : resolved ..
      }
   };

   var $ = obj;
   if (expr && obj) {
      P.trace(P.normalize(expr).replace(/^\$;?/,""), obj, "$");  // issue 6 resolved
      return P.result;
   }
   else {
      return null;
   }
};